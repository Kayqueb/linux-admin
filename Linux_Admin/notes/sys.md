---
title: 17 - Pseudo Filesystem /proc e /sys
created: '2021-03-31T02:00:00.377Z'
modified: '2021-03-31T02:31:59.174Z'
---

# 17 - Pseudo Filesystem /proc e /sys

 /proc --> Diretório virtual controlado pelo Kernel com configuração total do sistema 

 /sys --> Configurações dos dispostivos do sistema

 cmdline --> Linha de comando que foi executada no inicio do boot init
 
 environ --> onde fica as variáveis de ambiente do init

 status --> status da execução do processo

 acpi --> gerencia de energia e interrupções do disco

 acpi/wakeup --> status pra maquina voltar de um suspende

 cpuinfo --> dados sobre a cpu da máquina

 interrupts --> interrupções por cada hardware

 devices --> dispositivos sendo utilizados no sistema

 dma --> acesso direto a memória

 ioports --> portas de entrada e saída

 filesystems --> arquivos montados e suportados pelo sistema

 kcore --> representação da memória do sistema(apenas root consegue ler)

 cat kcore | less --> mostra tudo que está sendo usado pela memória(processos). Muito usado em análise forense

 obs: caso fique com erro na tela, só dar um reset no terminal

 loadavg --> contém os dados de load da máquina

 meminfo --> informações do uso de memória(mais completo)

 misc --> dados de módulos do sistema

  watchdog --> monitora o kernel, caso trave faz um reinicialização forçada

 modules --> os módulos que estão carregados no sistema(drivers)

 mounts --> ponto de montagem do sistema

 uptime --> uptime da máquina desde o último boot

 /sys --> dados sobre a conexões dos dispostivos do sistema

 bus --> mostra os barramentos da máquina. Os tipos de hardwares

 block --> dispostivos de blocos

 kernel --> dados de uso de recursos que dispositivos fazem no kernel. Pode fazer algumas alterações de uso

 





















