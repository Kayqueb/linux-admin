---
title: AULA DOCKER
created: '2021-04-29T21:20:10.839Z'
modified: '2021-04-30T17:47:39.451Z'
---

# AULA DOCKER 

- PREPARANDO O AMBIENTE

docker version --> mostra a versão do docker client e do server


docker container run --name web01 -p 80:80 nginx

docker container ls --> lista os containers que estão rondando

docker container ls -a --> mostra todos os containers

- INTRODUCAO AO CLI DO DOCKER

docker container ls -a --> mostra os containers all 

docker container --help --> mostra os comandos de gerenciamento 

docker container rm -f ID --> força pra remover um container rodando


- GERENCIANDO CONTAINERS DOCKER


Primeiro plano --> consegue acessar o console
Segundo --> perde o console

docker container run -p 8080:80 nginx

-p --> publica a porta do container para o host

ctrl+c --> sai termina o container

-d --> roda em segundo plano

-P --> pega todas as portas do container e vai publicar pro host docker de forma aleatória 

docker container run -d -p 8080 --name webhost nginx

--name -> nome do container

docker container logs webhost --> mostra os logs do container

docker container top webhost --> mostra os processos do container

docker  container stats --> mostra os dados do container(uso de cpu e mem)

docker  container inspect webhost --> mostra os detalhes do container em json

docker container inspect -f {{.NetworkSettings}} webhost --> traz as informações de rede


- CONCEITOS DE REDE DOCKER

Redes Bridge --> Rede padrão dos containers.

É feito um NAT. Melhores práticas é criar uma nova rede virtual para cada app. 

my_web = mysql,apache,php
my_api =flask,nodejs

docker network ls --> mostra as redes do containers

Redes Host --> Faz bridge. Utiliza a mesma rede da máquina. E não consegue publicar vários containers com a mesma porta. 

Não consegue rodar 2 nginx na mesma porta

Não consegue usar docker em modo swarm



Rede None --> Não possui acesso externo e nem de outros containers


docker network ls --> lista as redes

docker network create my_custom_net --> cria a rede padrão em bridge

docker network inspect my_custom_net --> traz as informaçoes da rede. Bom pra verificar se vai conflitar com seu ambiente.

docker network create my_custom_net2 --subnet 192.168.134.0/24 --gateway 192.168.134.1 --> cria uma rede que não vai conflitar com a rede

docker network rm my_custom_net --> remove a rede 

docker network prune --> remove todas as redes inativas


























