---
title: 65 - Gerenciamento de Logs - Criando um servidor de logs remoto
created: '2021-04-15T17:42:51.322Z'
modified: '2021-04-15T17:49:31.757Z'
---

# 65 - Gerenciamento de Logs - Criando um servidor de logs remoto

1 - descomentar UDP syslog
2 - descomentar TCP syslog

tcp --> envia e recebe
udp --> envia direto e não recebe

4 - vim /etc/rsyslog.d/syslog.conf

5- auth.*          @192.168.88.49
6 - systemctl restart syslog
