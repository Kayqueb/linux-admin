---
title: 26 - Administração de Usuários - Adicionando usuários
created: '2021-04-09T00:03:50.939Z'
modified: '2021-04-09T01:15:03.720Z'
---

# 26 - Administração de Usuários - Adicionando usuários

adduser --help --> boa pratica de informações

vim /etc/adduser.conf --> traz as configurações e informações básicas sobre o comando

skel=/etc/skel --> o esqueleto. Todos os arquivos que serão colocados no diretório home

Identificação do usuário
first_system_uid =100
last_system_uid =999


Grupos para sistemas
first_gid=100
last_gid=999

USERGROUPS=YES --> todo usuário que ele criar, será criado em um grupo (default)
USER_GID=100 --> Grupo 100

DIR_MODE=0755 --> Onde muda as permissões do diretório home

EXTRA_GROUPS --> Grupos secundários


adduser giropops --> criar usuário
id user --> mostra os detalhes do usuário
useradd jeferson --> criar o usuário, mas não mostra os detalhes
