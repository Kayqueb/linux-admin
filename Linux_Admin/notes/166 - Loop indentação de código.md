---
title: 166 - Loop indentação de código
created: '2021-05-20T23:10:40.863Z'
modified: '2021-05-24T15:15:59.137Z'
---

# 166 - Loop indentação de código

#!/bin/bsh 
#ls
echo -n "Entre com o nome de usuario: "
read USUARIO

if ["$USER" != "$USUARIO"]; then
    echo "Nao esta rodando como $USUARIO"
    exit 1
fi 

for VAR in $(seq 10 25); do
  echo " numero $VAR"
  mkdir /tmp/copia/dir$VAR #cria diretorios
done 

if [-d "/tmp/copia"]; then
    mkdir /tmp/copia
fi
if [! -f "/tmp/copia/ls"]; then
    cp /bin/ls /tmp/copia
fi
exit 0

---------------------------------
#!/bin/bsh 
#ls
echo -n "Entre com o nome de usuario: "
read USUARIO

if ["$USER" != "$USUARIO"]; then
    echo "Nao esta rodando como $USUARIO"
    exit 1
fi 

if [-d "/tmp/copia"]; then
    mkdir /tmp/copia
fi

for VAR in $(seq 10 25); do
  echo " numero $VAR"
  mkdir /tmp/copia/dir$VAR #cria diretorios
done 

if [! -f "/tmp/copia/ls"]; then
    cp /bin/ls /tmp/copia
fi
exit 0

obs: ele cria os diretorios e depois ao tentar novamente, retorna erro ao tentar criar 

--------------------------------------

#!/bin/bsh 
#ls
echo -n "Entre com o nome de usuario: "
read USUARIO

if ["$USER" != "$USUARIO"]; then
    echo "Nao esta rodando como $USUARIO"
    exit 1
fi 

if [-d "/tmp/copia"]; then
    mkdir /tmp/copia
fi

for VAR in $(seq 10 25); do
  if [ -d "/tmp/copia/dir$VAR"]; then
    echo "Diretorio /tmp/copia/dir$VAR existe, ignorando"
  else
    echo "não existe numero dir$VAR"
    mkdir /tmp/copia/dir$VAR #cria diretorios
  fi
done

if [! -f "/tmp/copia/ls"]; then
    cp /bin/ls /tmp/copia
fi
exit 0

obs: retorna os que existem e os que n existem


-------------------------------------------

#!/bin/bsh 
#ls
echo -n "Entre com o nome de usuario: "
read USUARIO

if ["$USER" != "$USUARIO"]; then
    echo "Nao esta rodando como $USUARIO"
    exit 1
fi 

if [-d "/tmp/copia"]; then
    mkdir /tmp/copia
fi

for VAR in $(seq 10 25); do
  if [ -d "/tmp/copia/dir$VAR"]; then
    echo "Diretorio /tmp/copia/dir$VAR existe, ignorando"
    if [ "$USER" = "guiafoca" ]; then
      echo " Rodando como guia foca"
  else
    echo "não existe numero dir$VAR"
    mkdir /tmp/copia/dir$VAR #cria diretorios
  fi
done

if [! -f "/tmp/copia/ls"]; then
    cp /bin/ls /tmp/copia
fi
exit 0













































