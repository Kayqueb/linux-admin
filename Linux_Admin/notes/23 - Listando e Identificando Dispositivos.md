---
title: 23 - Listando e Identificando Dispositivos
created: '2021-04-08T21:21:15.607Z'
modified: '2021-04-08T21:26:17.110Z'
---

# 23 - Listando e Identificando Dispositivos

lspci --> lista todos os periféricos que estão conectados no barramento PCI

lsusb --> lista dispositivos usb conectados 

lspci -vv --> mostra os detalhes de configurações

apt-get install lshw

lshw |less --> scannea os drivers em forma de árvore
