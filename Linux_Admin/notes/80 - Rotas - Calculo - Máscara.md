---
title: 80 - Rotas - Calculo - Máscara
created: '2021-04-17T12:57:23.172Z'
modified: '2021-04-17T13:05:02.937Z'
---

# 80 - Rotas - Calculo - Máscara

ip route ls --> lista as rotas utilizadas 
ip r ls
ip route list

apt-get install subnetcalc

subnetcalc 192.168.0.21/24 --> traz informações sobre a rota

subnetcalc fd00:f0ca:f0ca::fd1f/123
