---
title: 20 - Introdução ao LVM - Logical Volume Manager
created: '2021-04-01T19:20:57.844Z'
modified: '2021-04-07T19:29:39.474Z'
---

# 20 - Introdução ao LVM - Logical Volume Manager

Ajuste de forma dinamica com a máquina em funcionamento, sem precisar parar nada. 

partprobe /dev/sdb --> recarrega as tabelas de partições do disco

LVM
volume físico 
 grupo de volumes
volume lógico 
1 - ETAPA
cfdisk /dev/sdb 
 /dev/sdb1 --> LINUX LVM
 /dev/sdb2 --> LINUX LVM

2 - CRIAR VOLUME FISICO

apt-get install lvm2

pvcreate /dev/sdb1 --> volume fisico
y

pvs ou pvdisplay --> mostra os volumes

vgcreate vg-linuxexpert /dev/sdb1 --> cria o grupo de volumes

vgs ou vgdisplay --> mostra o grupo de volume

lvcreate -n linuxadmin -L 200M vg-linuxexpert
y
y
y

lvdisplay --> lista os volumes lógicos

mkdir /linuxadmin
mount /dev/vg-linuxexpert/linuxadmin /linuxadmin  --> monta o volume lógico em /linuxadmin

lvextend -L +200M /dev/vg-linuxexpert/linuxadmin

resize2fs /dev/vg-linuxexpert/linuxadmin --> Aponta pro sistema de arquivos pra fazer um resize online do kernel. E ajusta a partição


pvdisplay --> mostra o volume físico


-------------------------------
Reduzir é bem mais complicado. Tem que usar o espaço mínimo da criação de volumes lógicos e ir crescendo. Se utilizar uma partição ext4 com 64bits, vai pedir pra reduzir e se tiver menos de 16tb vc perde a flag de 64 bits e vai passar a trabalhar com 32bits. E não vai poder expandir o disco. E precisa desmontar a partição.


vgextend vg-linuxexpert /dev/sdb2 --> Associar o volume físico ao grupo de volumes.

lsblk --> lista o dispositivo de blocos e cada conexão dele em forma de árvore

lsblk -i --> mostra em asci2

lsblk -d --> mostra as capacidades de discarte

lsblk -l --> mostra em formato de lista




























