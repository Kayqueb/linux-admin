---
title: 42 - Permissões - O comando UMASK
created: '2021-04-12T22:57:23.007Z'
modified: '2021-04-12T23:06:54.597Z'
---

# 42 - Permissões - O comando UMASK

É com ele que sabemos a permissão padrão para um arquivo ou diretório

unix padrão
arquivo --> 666
dir --> 777

0666 - 0002 = 0664
0777 - 0002 = 0775

umask 0022 --> padrão pra modificar a umask

vim .profile --> modifico a umask padrão


