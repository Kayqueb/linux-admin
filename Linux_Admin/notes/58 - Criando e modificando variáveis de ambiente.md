---
title: 58 - Criando e modificando variáveis de ambiente
created: '2021-04-15T12:54:35.876Z'
modified: '2021-04-15T13:16:43.049Z'
---

# 58 - Criando e modificando variáveis de ambiente

printenv PATH --> traz o resultado da variável. Não precisa colocar o $

GIROPOPS=jeferson --> criar uma variável local

export GIROPOPS=jeferson --> transforma em uma variável válida

unset GIROPOPS --> desfaz o valor da variável 

set GIROPOPS=jeferson --> dá pra criar uma variável local

export GIROPOPS="Jeferson Fernando" --> Se tiver espaço, coloca entre aspas

export GIROPOPS="Jeferson Fernando":catota --> Se tiver mais de um valor, coloca : 

----------------------------





