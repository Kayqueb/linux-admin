---
title: 91 - Segurança - Restringindo usuários ssh
created: '2021-05-03T22:50:49.551Z'
modified: '2021-05-03T23:03:37.491Z'
---

# 91 - Segurança - Restringindo usuários ssh

cd /etc/ssh

ssh_config --> configuração do client

sshd_config --> configuração de servidor 

d -->daemon 

- ssh_config

AllowUsers guiafoca --> quem pode logar

PermitRootLogin yes --> permite logar como root

PermitRootLogin prohibit-password --> não vai permitir o login do root usando senha. Apenas chaves e certificados



PasswordAuthentication no --> vai proibir autenticação por senha. Apenas por chave publica ou certificado



sshd -t --> confirma se tem algum erro antes de restartar o serviço
















