---
title: 127 - CUPS - Adicionando PPD Manualmente
created: '2021-05-14T19:11:21.975Z'
modified: '2021-05-14T19:14:28.448Z'
---

# 127 - CUPS - Adicionando PPD Manualmente

lpadmin --> interface mais ativa

lpadmin -x lp --> removeu a impressora lp

lpstat -p PDF -l --> status 

lpadmin -p lp -E socket://ip -m Laserjet.ppd --> adiciona a impressora

