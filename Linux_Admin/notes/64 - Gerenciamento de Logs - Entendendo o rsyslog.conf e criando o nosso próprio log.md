---
title: 64 - Gerenciamento de Logs - Entendendo o rsyslog.conf e criando o nosso próprio log
created: '2021-04-15T14:56:01.218Z'
modified: '2021-04-15T17:36:54.910Z'
---

# 64 - Gerenciamento de Logs - Entendendo o rsyslog.conf e criando o nosso próprio log

vim /etc/rsyslog.conf

rm /etc/rsyslog.conf.sw --> remover quando estiver alguém usando rsyslog(travado)
--------------
vim /etc/rsyslog.conf

auth.debug       /var/log/giropops.log

auth--> facilidade, serviço, tipo de log

debug --> prioridade

facilidade.prioridade --------- caminho do arquivo log

auth --> mensagens de seg e autorização

authpriv --> mensagens de seg e auto(priv)

cron --> daemons de agendamento 

daemon --> outros daemons do sistema que n possuem facilidades específicas

ftp --> daemon de ftp

kern --> mensagens do kernel

lpr --> subsistema de impressão

loca10 a loca1 --> reservados para uso local

mail --> subsistema de email

news --> subsist de noticias da usene

security --> sinomimo para auth(evitar)

syslog --> mensagens internas geradas pelo syslogd

user --> mensagens genéricas de nível do usuário

uucp --> subsistema de uucp

* --> confere com todas as facilidades

- NÍVEL 

emerg --> sistema inutilizável 
alert --> uma ação precisa ser tomada
crit --> critico
erro --> erros
warning --> alertas
notice --> normal
info --> informativas
debug --> depuração
* --> confere todos os níveis
none --> nenhuma

 - Sinonimos 

erros --> pra err
panic --> emerg
warn --> warning















