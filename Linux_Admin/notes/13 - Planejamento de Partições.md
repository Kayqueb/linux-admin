---
title: 13 - Planejamento de Partições
created: '2021-03-24T02:49:49.147Z'
modified: '2021-03-25T18:53:39.734Z'
---

# 13 - Planejamento de Partições

- Disco 1TB
Debia 10 / Ubuntu 20

Desktop 
/ - 20GB
/usr - 15Gb
/var - 10Gb
/home -500Gb
- swap - 8Gb


Mais simples:
/ 50gb
/home - 930gb
-swap - 8gb

Servidor de E-mails
/ 20gb
/var 900gb (/var/email)
/home 5gb
-swap 16gb

Sevidor de Arquivos
/ 20gb 
/home 50gb
/data 900gb
-swap 8gb

Servidor web
/ 20gb
/home 5gb
-swap 8gb
/var 950 gb
 - /var/www
 - /var/log

 mkdir /tmp/mount_var --> pasta de ponto de montagem
 mount /dev/sda6(dispositivo de origem) /tmp/mount_var(ponto de montagem)

Como migrar? (migração aquente)
1- Entrar na pasta de destino 
 cd /var/tmp/mount_var/
 cp -a /var/* /tmp/mount_var

 Caso apague a pasta lost+found

 mklost+found --> cria novamente

mcedit -b /etc/fstab
   /dev/sda6 /var ext4  defaults   0   1(verifica o sistema de arquivo) e o 0 (ignora)
cd /var/
rm -rf*
reboot

Ex: mcedit -b /etc/fstab
   /dev/sda6 /var ext4  defaults   0

opções <options>

noatime --> não atualiza o acess time dos arquivos e reduz a gravação de sistemas de arquivos. 

nodiratime --> serve para diretórios

norelatime --> serve pra inodes(usar com noatime) 

noexec --> qualquer binário que estiver no /var não tem opção de execução. Ex: Não deixar criar dispositivos que podem ser inseguros

noexec, nosuid, 

nodev --> não pode criar nenhum dispositivo/especial

/dev/null --> buraco negro

lsof -->  mostra o que está em execução 

lsof -t /var --> mostra o que está em execução na pasta

















