---
title: 76 - Traceroute6 e mtr
created: '2021-04-16T13:34:59.850Z'
modified: '2021-04-16T13:37:40.350Z'
---

# 76 - Traceroute6 e mtr

traceroute6 www.google.com --> utilizado pra ipv6

mtr -6 wwww.google.com --> utilizado pra ipv6

Para verificar perdas é melhor usar o traceroute
