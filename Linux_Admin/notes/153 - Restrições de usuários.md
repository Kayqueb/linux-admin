---
title: 153 - Restrições de usuários
created: '2021-05-20T21:29:52.048Z'
modified: '2021-05-20T21:33:42.409Z'
---

# 153 - Restrições de usuários

cd /etc/ssh/

vi sshd_config

AllowUsers root user1

Se tirar o user, não consegue conectar

DenyUsers user2 --> Permite todos, menos user2

AllowGroups grupo




