---
title: 69 - Protocolos e o modelo OSI
created: '2021-04-15T21:51:08.609Z'
modified: '2021-04-15T22:25:24.914Z'
---

# 69 - Protocolos e o modelo OSI




7 Aplicação --> HTTP, FTP, Navegador. Responsável por prover o serviço. 

6 Apresentação --> Responsável pela formatação das informações para entregar a aplicação. Ex: TLS

5 Sessão --> Inicia e analisa se está tudo funcionando. Coordena o fluxo. Ex: NetBios 

4 Transporte --> TCP(motorista) e UDP(baú). Envia e recebe(tcp). Só enviar(UDP).

3 Rede --> IP, IPX, IPSEC,NAT. Define o melhor caminho. Roteadores Switches layers 3,

2 Enlace --> Dividida em 2(Mac e LLC-Logic Link Control). Switches, ethernet, Token Ring

1 Fisica  --> Bluetooth, Wifi, Ethernet. O Hardware que trabalha.
