---
title: 'Iniciando, parando, executando e removendo containers'
tags: [Import-255a]
created: '2021-06-13T12:49:51.029Z'
modified: '2021-06-13T12:52:13.162Z'
---

---
title: 'Iniciando, parando, executando e removendo containers'
created: '2021-05-24T20:25:04.482Z'
modified: '2021-05-24T20:31:26.908Z'
---

# Iniciando, parando, executando e removendo containers

- Instalando

curl -sSL https://get.docker.com | sh

ce --> versão mais comum
e --> redhat 


docker container ls --> lista os containers

docker image ls --> lista as imagens

docker container run --publish 8080:80 --name nginx nginx --> instalar nginx na porta 80 com o nome nginx

docker container start nginx --> iniciar o nginx

docker container stop id --> para 

docker container rm id --> remove o container
