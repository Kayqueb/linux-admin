---
title: 168 - Fluxo usando o case
tags: [Import-255a]
created: '2021-06-13T12:49:51.029Z'
modified: '2021-06-13T12:52:07.864Z'
---

---
title: 168 - Fluxo usando o case
created: '2021-05-24T15:19:54.642Z'
modified: '2021-05-24T18:22:54.578Z'
---

# 168 - Fluxo usando o case
./checa_valor

#!/bin/bash

echo "Entre com o comando"
read VALOR

# bloco captura start e finaliza
case "$VALOR" in
  start)
    echo "Foi digitado o comando start"
  ;;
# bloco captura stop e finaliza
  case "$VALOR" in
  stop)
    echo "Foi digitado o comando stop"
  ;;
# bloco pede pra digitar uma opção valida
  *)
    echo "Por favor, digite APENAS start ou stop"
    exit 1
  ;;
esac
echo "Fim do programa"
exit 0

----------------------------------------
./checa_valor

#!/bin/bash

# echo "Entre com o comando"
# read VALOR

if [ -z "$1"]; then
  echo "Por favor, digite um parametro no primeiro argumento"
  exit 1
fi
if [ -z "$1"]; then
  echo "Por favor, digite um parametro no segundo argumento"
  exit 1
fi

echo "Nome do programa: $0"
echo "Primeiro argumento: $1"
echo "Segundo argumento: $2"

exit 0

VALOR=$1

# bloco captura start e finaliza
case "$VALOR" in
  start)
    echo "Foi digitado o comando start"
  ;;
# bloco captura stop e finaliza
  case "$VALOR" in
  stop)
    echo "Foi digitado o comando stop"
  ;;
# bloco pede pra digitar uma opção valida
  *)
    echo "Por favor, digite APENAS start ou stop"
    exit 1
  ;;
esac
echo "Fim do programa"
exit 0
