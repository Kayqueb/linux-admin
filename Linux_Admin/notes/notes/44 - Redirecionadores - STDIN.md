---
title: 44 - Redirecionadores - STDIN
tags: [Import-255a]
created: '2021-06-13T12:49:51.053Z'
modified: '2021-06-13T12:51:50.180Z'
---

---
title: 44 - Redirecionadores - STDIN
created: '2021-04-13T00:33:29.596Z'
modified: '2021-04-13T00:44:13.232Z'
---

# 44 - Redirecionadores - STDIN

cat > opa.txt

ele pede pra digitar pra enviar pro arquivo. Dá um ctrl + d. Pronto.


< --> Redireciona a entrada padrão - stdin

<< --> Redireciona a entrada padrão 

Ex: cat << giropops

Obs: Você pode digitar um texto e ele vai esperar até chegar na palavra giropops. E depois imprime na tela.


Ex: cat << giropops > arquivo.txt 

Obs: Vai digitar o texto e ele vai jogar pro arquivo. E substitui o conteúdo do arquivo. 

Ex: cat << giropops >> arquivo.txt

Obs: Vai digitar o texto e ele vai jogar pro arquivo. E concatenando. 

cat << EOF >> arquivo.txt

EOF --> end of file

Isso é usado como convenção. Indica o fim do arquivo.
