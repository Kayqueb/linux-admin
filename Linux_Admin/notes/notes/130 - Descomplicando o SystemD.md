---
title: 130 - Descomplicando o SystemD
tags: [Import-255a]
created: '2021-06-13T12:49:51.037Z'
modified: '2021-06-13T12:52:02.278Z'
---

---
title: 130 - Descomplicando o SystemD
created: '2021-05-18T21:18:51.525Z'
modified: '2021-05-18T21:58:10.257Z'
---

# 130 - Descomplicando o SystemD

runlevel --> mostra o runlevel atual

systemctl restart ssh --> restarta o serviço

status --> mostra o status

stop --> stopa

systectl isolate rescue --> modo de manutenção

Se não voltar a placa de rede 

systemctl isolate multi-user.target

systemctl get-default --> mostra o default

systemctl set-default graphical.target --> vai setar o graphical

cd /etc/systemd/system --> local onde fica os services a serem startados. Ficam em seus diretórios
