---
title: 148 - Criando chave publi⁄priv
tags: [Import-255a]
created: '2021-06-13T12:49:51.033Z'
modified: '2021-06-13T12:52:13.763Z'
---

---
title: 148 - Criando chave publi/priv
created: '2021-05-20T20:44:43.393Z'
modified: '2021-05-20T20:47:34.586Z'
---

# 148 - Criando chave publi/priv

ssh-keygen --> gera e salva no .ssh/id_rsa

pede a frase senha: coloca a frase

publi é a chave que será utilizada em outras máquinas

ssh-copy-id usuario@ip --> vai adicionar a chave na máquina que será conectada
