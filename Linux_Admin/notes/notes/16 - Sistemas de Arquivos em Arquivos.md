---
title: 16 - Sistemas de Arquivos em Arquivos
tags: [Import-255a]
created: '2021-06-13T12:49:51.049Z'
modified: '2021-06-13T12:51:47.653Z'
---

---
title: 16 - Sistemas de Arquivos em Arquivos
created: '2021-03-30T22:47:03.584Z'
modified: '2021-03-31T01:57:44.568Z'
---

# 16 - Sistemas de Arquivos em Arquivos
mkdir data
cd /data
truncate -s 10T teste.img
mkfs.ext4 teste.img
mount teste.img /mnt

swapon -p -3 teste.img  --> muda a prioridade da partição

echo 1 /proc/sys/vm/swappiness --> evita que o ssd não se desgaste muito e usa o swap no ultimo caso
