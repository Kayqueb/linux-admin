---
title: 173 - Includes - Tornando código escalável
tags: [Import-255a]
created: '2021-06-13T12:49:51.025Z'
modified: '2021-06-13T12:52:09.428Z'
---

---
title: 173 - Includes - Tornando código escalável
created: '2021-05-24T19:14:02.812Z'
modified: '2021-05-24T19:18:24.685Z'
---

# 173 - Includes - Tornando código escalável

vim bibliotecas_funcoes

funcao1() {
  echo "oi, você chamou a funcao1, com o argumento $1, oque pode\
  lhe ser util?"
}

funcao2() {
  echo "Ei, sou a funcao2, chamada com o argumento $1, o que vamos fazer?"
}

-------

#!/bin/bash
# Este programa é um teste de funçao 
lista_diretorios() {
  ls $1
  echo "A listagem de $1 foi executada!"
}
. biblioteca_funcoes #inclue os arquivos aqui

for dir in $1 $2 $3 $4 $5 $6 $7 $8 $9 do
  #lista_diretorio $dir
  funcao1 $dir
  funcao2 $dir
done


#ls $1
ls $2
ls $3
ls $4
ls $5
ls $6
ls $7#

# teste_funcao /bin /sbin /usr /tmp
