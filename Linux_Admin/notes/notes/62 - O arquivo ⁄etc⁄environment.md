---
title: 62 - O arquivo ⁄etc⁄environment
tags: [Import-255a]
created: '2021-06-13T12:49:51.041Z'
modified: '2021-06-13T12:52:12.975Z'
---

---
title: 62 - O arquivo /etc/environment
created: '2021-04-15T13:57:43.059Z'
modified: '2021-04-15T13:58:59.052Z'
---

# 62 - O arquivo /etc/environment

Local onde guarda as variáveis de ambiente e será lido toda vez que iniciar a máquina.
