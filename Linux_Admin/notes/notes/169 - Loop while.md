---
title: 169 - Loop while
tags: [Import-255a]
created: '2021-06-13T12:49:51.025Z'
modified: '2021-06-13T12:52:07.934Z'
---

---
title: 169 - Loop while
created: '2021-05-24T18:23:41.956Z'
modified: '2021-05-24T18:28:26.553Z'
---

# 169 - Loop while
#!/bin/bash

cat /etc/paswd | while read linha/ do
  echo $linha
done

echo "Entre com o comando"
read VALOR

echo "Nome do programa: $0"
echo "Primeiro argumento: $1"
echo "Segundo argumento: $2"

exit 0

VALOR=$1

# bloco captura start e finaliza
case "$VALOR" in
  start)
    echo "Foi digitado o comando start"
  ;;
# bloco captura stop e finaliza
  case "$VALOR" in
  stop)
    echo "Foi digitado o comando stop"
  ;;
# bloco pede pra digitar uma opção valida
  *)
    echo "Por favor, digite APENAS start ou stop"
    exit 1
  ;;
esac
echo "Fim do programa"
exit 0
