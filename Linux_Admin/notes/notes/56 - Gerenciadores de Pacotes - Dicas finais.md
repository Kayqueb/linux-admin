---
title: 56 - Gerenciadores de Pacotes - Dicas finais
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:51.316Z'
---

---
title: 56 - Gerenciadores de Pacotes - Dicas finais
created: '2021-04-15T12:01:42.175Z'
modified: '2021-04-15T12:03:30.270Z'
---

# 56 - Gerenciadores de Pacotes - Dicas finais

apt-get install -y wget --> instala direto 

apt-get autoremove --> remove tudo o que você não precisa

apt-get install -f --> pega tudo que deu errado na instalação e instala as dependencias
