---
title: 140 - Verificando e isolando Badblocks
tags: [Import-255a]
created: '2021-06-13T12:49:51.033Z'
modified: '2021-06-13T12:52:04.023Z'
---

---
title: 140 - Verificando e isolando Badblocks
created: '2021-05-20T20:01:25.813Z'
modified: '2021-05-20T20:06:10.288Z'
---

# 140 - Verificando e isolando Badblocks

Não sendo na trilha zero

badblocks --help

badblocks -d --> simular um delay na leitura

badblocks -n --> mostra o problema

badblocks -w --> mostra e resolve(modo destrutivo/apaga a partição)

badblocks -sv -c 1024 /dev/sdb1 --> checa o bloco em 1024 blocos
