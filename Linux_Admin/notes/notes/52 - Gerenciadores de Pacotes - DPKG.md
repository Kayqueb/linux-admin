---
title: 52 - Gerenciadores de Pacotes - DPKG
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:50.983Z'
---

---
title: 52 - Gerenciadores de Pacotes - DPKG
created: '2021-04-14T20:53:20.236Z'
modified: '2021-04-14T22:28:20.262Z'
---

# 52 - Gerenciadores de Pacotes - DPKG

dpkg --> debian pakage

dpkg nome do pacote.deb -i --> pra instalar 

dpkg -l --> traz todos os pacotes instalados e a versão

dpkg -l bb --> traz as informações do app

dpkg -r bb --> remove o pacote
 rc --> restos do pacote

dpkg -P bb --> remove tudo com o PURGE

dpkg -S /usr/games/bb -->

dpkg -L bb --> lista todos os arquivos que pertence ao bb

dpkg -I bb.deb --> informações relacionadas ao pacote

dpkg -s bb --> traz o status do app já instalado

dpkg -C bb --> mostra problemas de pacote

dpkg --configure pacote --> configura algum pacote que esteja com problema
