---
title: 10 - Gerenciamento e execução de processos
tags: [Import-255a]
created: '2021-06-13T12:49:51.049Z'
modified: '2021-06-13T12:51:46.554Z'
---

---
title: 10 - Gerenciamento e execução de processos
created: '2021-03-18T20:39:03.514Z'
modified: '2021-03-19T22:20:32.228Z'
---

# 10 - Gerenciamento e execução de processos

ps --> exibe os processos executando no terminal atual 
ps -a --> exibe todos os processos sendo executados em vários terminais
ps -ax --> exibe todos os processos de outros usuários e não associados pelo sistema
ps -u --> exibe os usuários dos processos
ps -axm --> exibe status de memória
ps -axf --> exibe em modo de árvore
ps -axe --> mostra as variáveis de ambiente
ps -axew --> mostra a saída inteira do processo com quebra de linha
ps axu --sort=pid --> organiza por pid

pidof --> exibe o pid do processo. ex: pidof ssh
pstree --> exibe a árvore de processos de forma gráfica

kill --> matar o processo
kill -15 --> sinal terminate de forma amigavel 
kill -9 --> ele termina na hora( muito cuidado em BD)
kill -HUP 1846 --> vai reler a configuração
kill %1 --> mata em segundo plano
pgrep -u root sshd --> grep em processos 
pgrep sshd --> procura em todos os usuários
pkill --> manipula os processos por nome
killall --> mata o processo de acordo com o nome
killall5 --> mata todos os processos(root)




PID --> Process identification 

sleep
jobs --> mostra processos em segundo plano
fg 1 --> foreground(primeiro plano)
bg 1 --> jogo pra segundo plano, mas preciso pausar o processo.

kill  %3(job) --> mata o processo em segundo plano 


top --> mostra todos os processos rodando
top -i --> ignora processos zumbis
top -c --> mostra a linha de comando completa


nice --> prioridade que um programa será executado
-20 --> maior prioridade
-19 --> menor prioridade

nice -n 10 programa --> edita a prioridade de execução 

nice -n -2 programa --> só root consegue colocar a prioridade maior

renice -n 10 -p 597 --> ajustará a prioridade nova como 10

tload --> permite monitorar os processos de forma gráfica em modo texto

vmstat 1 --> atualizar os processos em 1 segundo
