---
title: 164 - Exit codes - Variaveis de ambiente
tags: [Import-255a]
created: '2021-06-13T12:49:51.021Z'
modified: '2021-06-13T12:52:07.492Z'
---

---
title: 164 - Exit codes - Variaveis de ambiente
created: '2021-05-20T22:26:49.501Z'
modified: '2021-05-20T22:39:05.067Z'
---

# 164 - Exit codes - Variaveis de ambiente

./copia_rquivos
echo $? 
#0

Exit code é o último comando exec

#!/bin/bsh 
#ls
if ["$(whoami)" = "root"]; then
    echo "Executando como root"
    else
    echo "Nao esta rodando como root"
fi 

exit 0

if [-d "/tmp/copia"]; then
    mkdir /tmp/copia
fi
if [! -f "/tmp/copia/ls"]; then
    cp /bin/ls /tmp/copia
fi
exit 0

obs: ele vai rodar até o primeiro exit code

--------------------------------

#!/bin/bsh 
#ls
if ["$(whoami)" != "root"]; then
    echo "Nao esta rodando como root"
    exit 1
fi 

exit 0

if [-d "/tmp/copia"]; then
    mkdir /tmp/copia
fi
if [! -f "/tmp/copia/ls"]; then
    cp /bin/ls /tmp/copia
fi
exit 0


obs: vai executar e caso n for root, sai e mostra o erro 

------------------------------------------

set --> usado pra variaveis de ambientes

set |less --> mostra as variaveis q pode utilizar

#!/bin/bsh 
#ls
if ["$USER" != "root"]; then
    echo "Nao esta rodando como root"
    exit 1
fi 

exit 0

if [-d "/tmp/copia"]; then
    mkdir /tmp/copia
fi
if [! -f "/tmp/copia/ls"]; then
    cp /bin/ls /tmp/copia
fi
exit 0

OBS: Ele vai verificar pela variável de ambiente $USER


echo $? --> mostra o ultimo exit code
