---
title: 153 - Restrições de usuários
tags: [Import-255a]
created: '2021-06-13T12:49:51.033Z'
modified: '2021-06-13T12:52:06.284Z'
---

---
title: 153 - Restrições de usuários
created: '2021-05-20T21:29:52.048Z'
modified: '2021-05-20T21:33:42.409Z'
---

# 153 - Restrições de usuários

cd /etc/ssh/

vi sshd_config

AllowUsers root user1

Se tirar o user, não consegue conectar

DenyUsers user2 --> Permite todos, menos user2

AllowGroups grupo
