---
title: 72 - IPs para Redes Privadas
tags: [Import-255a]
created: '2021-06-13T12:49:51.053Z'
modified: '2021-06-13T12:51:53.255Z'
---

---
title: 72 - IPs para Redes Privadas
created: '2021-04-15T23:36:48.384Z'
modified: '2021-04-15T23:40:09.955Z'
---

# 72 - IPs para Redes Privadas

Específicos para usar na rede local. 

10.0.0.0/8  - 1 octeto para rede

172.16.0.0/12  - 4 primeiros da rede

192.168.0.0/16  - 2 primeiros octetos da rede
