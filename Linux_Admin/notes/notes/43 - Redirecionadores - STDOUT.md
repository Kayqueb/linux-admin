---
title: 43 - Redirecionadores - STDOUT
tags: [Import-255a]
created: '2021-06-13T12:49:51.053Z'
modified: '2021-06-13T12:51:50.107Z'
---

---
title: 43 - Redirecionadores - STDOUT
created: '2021-04-13T00:23:48.703Z'
modified: '2021-04-13T00:32:26.765Z'
---

# 43 - Redirecionadores - STDOUT 

> --> Redireciona a saída padrão - stdout - standard output. 

Por exemplo o ls, ao invés de ir pra tela, ele vai enviar para um arquivo

ls > saida_ls.txt

obs: Ele sobrescreve o que estiver dentro do arquivo. Muito cuidado!!


>> --> Redireciona a saída padrao e concatena em arquivo/dispositivo indicado. 

ls >> saida_ls.txt

Ele não substitui o arquivo. Ele junta ao original.
