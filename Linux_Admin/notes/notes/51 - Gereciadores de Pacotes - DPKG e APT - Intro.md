---
title: 51 - Gereciadores de Pacotes - DPKG e APT - Intro
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:50.899Z'
---

---
title: 51 - Gereciadores de Pacotes - DPKG e APT - Intro
created: '2021-04-14T20:48:46.939Z'
modified: '2021-04-14T20:52:56.787Z'
---

# 51 - Gereciadores de Pacotes - DPKG e APT - Intro

dpkg --> Para fazer instalação desde que o pacote esteja no pc. E não resolve dependencias. 

Arquivo .deb

APT --> Se estiver no repositorio do apt get, o pacote será baixado pelo endereço. O apt get chama o dpkg para instalar.
Ele resolve dependecias.
