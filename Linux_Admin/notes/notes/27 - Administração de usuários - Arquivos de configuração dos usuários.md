---
title: 27 - Administração de usuários - Arquivos de configuração dos usuários
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:48.275Z'
---

---
title: 27 - Administração de usuários - Arquivos de configuração dos usuários
created: '2021-04-09T01:17:12.485Z'
modified: '2021-04-09T01:43:20.817Z'
---

# 27 - Administração de usuários - Arquivos de configuração dos usuários


cat /etc/passwd

root:x:0:root:/root:/bin/bash -->
nomedousuario:senha está em outro lugar:uid:gid:informacoes do usuario:/home do usuario: shell

obs: Não é recomendado editar o passwd

cat /etc/shadown --> local onde ficam o hash das senhas dos usuários
