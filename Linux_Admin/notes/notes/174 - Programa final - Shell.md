---
title: 174 - Programa final - Shell
tags: [Import-255a]
created: '2021-06-13T12:49:51.025Z'
modified: '2021-06-13T12:52:09.535Z'
---

---
title: 174 - Programa final - Shell
created: '2021-05-24T19:26:14.203Z'
modified: '2021-05-24T19:32:16.111Z'
---

# 174 - Programa final - Shell

#programa vai executar a instalação de pacotes em sequencia 

vi instala_exec_nginx

#!/bin/sh

if [ -z "$1"]; then
  echo "Necessario especificar um nome de pacote para ser instalado
  exit 1
fi

if ["$(whoami)" != "root"]; then
  echo "Necessario usuario root $1"
  exit 2
fi

apt update &&\
apt -y --no-install-recommends install nginx && \
echo "Nginx instalado com sucesso"
exit 0
