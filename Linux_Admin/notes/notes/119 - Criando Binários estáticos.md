---
title: 119 - Criando Binários estáticos
tags: [Import-255a]
created: '2021-06-13T12:49:51.037Z'
modified: '2021-06-13T12:52:00.235Z'
---

---
title: 119 - Criando Binários estáticos
created: '2021-05-10T21:40:37.880Z'
modified: '2021-05-10T21:53:26.850Z'
---

# 119 - Criando Binários estáticos

ldd hello --> mostra as bibliotecas usadas pra exec 

gcc -wall -static hello.c -o hello --> de forma estatica

ldd hello --> não possui bibliotecas compartilhadas
