---
title: 8 - Coringas
tags: [Import-255a]
created: '2021-06-13T12:49:51.049Z'
modified: '2021-06-13T12:51:46.440Z'
---

---
title: 8 - Coringas
created: '2021-02-03T22:35:08.341Z'
modified: '2021-02-24T23:42:14.376Z'
---

# 8 - Coringas

* --> nenhum, um ou todos. Ex: ls a* 
Exibe todos que começam com a letra a

? --> Ex: ls m? --> Exibe todos que começam com a letra m e tenham um caracter após essa posição

ls m?t* --> Exibe todos que começam com m, tenham um caracter após o m, tenha t e tenha mais um caracter após essa posição

[] --> delimita uma faixa de caracteres. 
Ex: ls m[a-c]

{} --> pedaços do arquivo 
Ex: ls x{zd,ze,zm} --> Exibe arquivos que contenham essas letras
