---
title: 54 - Gerenciadores de Pacotes - APT
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:51.070Z'
---

---
title: 54 - Gerenciadores de Pacotes - APT
created: '2021-04-14T22:28:57.728Z'
modified: '2021-04-14T22:47:53.624Z'
---

# 54 - Gerenciadores de Pacotes - APT 

apt-get install --> vai até o repositório, baixa e instala 

apt-get remove --purge bb--> pra remover tudo relacionado

apt-get update --> vai nos repositórios e faz o download da lista de pacotes mais recentes

  cd /etc/apt
  source.list --> onde coloca os repos
  source.list.d --> local pra colocar novos repos que não seja padrão
