---
title: 120 - Compilação - GDB - Strace - Strip
tags: [Import-255a]
created: '2021-06-13T12:49:51.037Z'
modified: '2021-06-13T12:52:00.852Z'
---

---
title: 120 - Compilação - GDB - Strace - Strip
created: '2021-05-10T21:43:15.003Z'
modified: '2021-05-10T21:53:52.250Z'
---

# 120 - Compilação - GDB - Strace - Strip

strace .hello --> ele vai mostrar todas as funções pra executar


gdb .hello --> abrir o binário e fazer o rastreio. Engenharia reversa

strip hello --> vai proteger contra o gdb
