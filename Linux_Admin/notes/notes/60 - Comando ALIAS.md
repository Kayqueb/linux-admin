---
title: 60 - Comando ALIAS
tags: [Import-255a]
created: '2021-06-13T12:49:51.041Z'
modified: '2021-06-13T12:51:52.244Z'
---

---
title: 60 - Comando ALIAS
created: '2021-04-15T13:25:21.940Z'
modified: '2021-04-15T13:30:42.173Z'
---

# 60 - Comando ALIAS

alias listar="ls -lha" --> Ao digitar listar, o comando ls -lha será executado

alias --> exibe os alias criado

Se fechar o terminal, pode perder o Alias.

unalias listar --> remove o alias
