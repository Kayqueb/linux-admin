---
title: 67 - Gerenciamento de Logs - Descomplicando o Journalctl
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:52.734Z'
---

---
title: 67 - Gerenciamento de Logs - Descomplicando o Journalctl
created: '2021-04-15T18:23:13.058Z'
modified: '2021-04-15T21:17:01.100Z'
---

# 67 - Gerenciamento de Logs - Descomplicando o Journalctl

journalctl --> traz as mensagens 

q --> sai do journal

journalctl -b --> traz as mensagens do ultimo boot

journalctl -b --utc --> traz com universal time 

journalctl -b --since "2020-12-19 07:00" --until "2020-12-19 07:10" --> define um intervalo 

jornalctl --since today

journalctl --since today --until "2 hours ago"

journalctl -u ssh --> mostra mensagens ssh 

-u é um cara do unix. Um serviço


journalctl -k --> kernel 

journalctl -k -p emerg --> emergencias

journalctl -a --> all

journalctl -k -o json --> traz no formato de json

journalctl -k -o json-pretty --> traz mais organizado 

journalctl --disk-usage --> mostra o quanto usado no disco

journalctl --vacuum-size=2m --> ele define o tamanho do journal e limpa caso passe 

journalctl --vacum-time=4weeks

/etc/systemd/journald.conf --> config do journal

 -systemmaxuse --> tamanho do disco
 -systemkeepfree --> espaço do journal que ficará livre


logger " GIROPOPS " --> gera uma mensagem de log
tail -f /var/log/messages
