---
title: 132 - Os arquivos de configuração do GRUB
tags: [Import-255a]
created: '2021-06-13T12:49:51.033Z'
modified: '2021-06-13T12:52:03.195Z'
---

---
title: 132 - Os arquivos de configuração do GRUB
created: '2021-05-18T22:15:37.955Z'
modified: '2021-05-18T22:26:11.829Z'
---

# 132 - Os arquivos de configuração do GRUB

cd /etc/grub.d

arquivos q não podem alterar, pois vão criar os arquivos cfg

40_custom --> único que pode alterar

cd /boot 

vmlinuz --> kernel

initrd --> utilizado no boot

etc /boot/grub

vim grub.cfg --> gerados automaticamente 

ro --> read only
