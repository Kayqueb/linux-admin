---
title: 31 - Administração de Usuários - Removendo grupos
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:48.605Z'
---

---
title: 31 - Administração de Usuários - Removendo grupos
created: '2021-04-09T14:18:07.082Z'
modified: '2021-04-09T14:20:53.993Z'
---

# 31 - Administração de Usuários - Removendo grupos

delgroup --help --> opções para remover 

delgroup catota --> remove o grupo
groupdel girus --> remove o grupo

obs: Se você tem um membro primário dentro do grupo, você não consegue remover. Precisa remover o usuário primeiro.
