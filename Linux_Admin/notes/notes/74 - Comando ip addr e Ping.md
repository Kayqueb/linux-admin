---
title: 74 - Comando ip addr e Ping
tags: [Import-255a]
created: '2021-06-13T12:49:51.053Z'
modified: '2021-06-13T12:51:53.432Z'
---

---
title: 74 - Comando ip addr e Ping
created: '2021-04-16T13:17:28.693Z'
modified: '2021-04-16T13:29:52.613Z'
---

# 74 - Comando ip addr e Ping

ip addr show --> lista as interfaces

lo --> loopback. Interface local.

enp0s3 --> comunicação externa

ens192 --> vmware

ip addr show dev enp0s3 --> lista apenas a interface

ping6 --> pingar no ip

ip -6 addr show dev enp0s3 --> lista apenas ipv6

ip addr add 192.168.0.215/24 dev enp0s3 --> adiciona o ip na interface

latencia --> ida e retorno num destino. Quanto menor, melhor.
