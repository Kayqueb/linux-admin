---
title: 81 - Rotas - Multiplas - Redes
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:54.569Z'
---

---
title: 81 - Rotas - Multiplas - Redes
created: '2021-04-17T13:26:12.546Z'
modified: '2021-04-17T13:32:52.611Z'
---

# 81 - Rotas - Multiplas - Redes

Fazendo a máquina pingar numas redes

ip addr add 192.168.1.2/24 dev enp0s3 --> adicionando uma segunda rede

Funciona numa rede ponto a ponto. Numa mesma placa física.

ip addr add 192.168.2.4/24 dev enp0s3
ip add s dev enp0s3

Adiciona uma terceira rede

Obs: Não se preocupe com a quantidade de rotas, mas planeje bem.
