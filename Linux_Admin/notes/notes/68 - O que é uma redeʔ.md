---
title: 68 - O que é uma redeʔ
tags: [Import-255a]
created: '2021-06-13T12:49:51.053Z'
modified: '2021-06-13T12:51:52.819Z'
---

---
title: 68 - O que é uma rede?
created: '2021-04-15T21:25:28.972Z'
modified: '2021-04-15T21:41:16.837Z'
---

# 68 - O que é uma rede?

Um ou mais computadores conectados. 

Host --> Tudo aquilo que tem um ip ou está na rede. 

Protocolo --> São regras para comunicação com outro computador 

IP --> idenficação na rede do pc

Netmask --> define a porção da sua rede que será destinado a rede e ao host. Ex: 255.255.255.0

Network --> Endereço da rede. Ex: 192.168.68.0

Broadcast --> Endereço que pertence a toda rede, Ex: 192.168.86.255

Gateway --> É o que te leva pra internet ou redes. Ex: 192.168.86.1
