---
title: 175 - Iniciando containers em background
tags: [Import-255a]
created: '2021-06-13T12:49:51.025Z'
modified: '2021-06-13T12:52:09.636Z'
---

---
title: 175 - Iniciando containers em background
created: '2021-05-24T20:32:00.360Z'
modified: '2021-05-24T20:33:44.853Z'
---

# 175 - Iniciando containers em background 

docker container run --publish 8080:80 -d --name nginx nginx --> desconecta do container e deixa ele em segundo plano

docker container logs nginx --> mostra o logs
