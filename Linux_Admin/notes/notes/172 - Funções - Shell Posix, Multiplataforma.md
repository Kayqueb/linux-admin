---
title: '172 - Funções - Shell Posix, Multiplataforma'
tags: [Import-255a]
created: '2021-06-13T12:49:51.029Z'
modified: '2021-06-13T12:52:09.289Z'
---

---
title: '172 - Funções - Shell Posix, Multiplataforma'
created: '2021-05-24T18:52:54.382Z'
modified: '2021-05-24T19:13:56.818Z'
---

# 172 - Funções - Shell Posix, Multiplataforma

função --> permite q reutilize parte do seu código

#!/bin/bash
# Este programa é um teste de funçao 
function lista_diretorios() {
  ls $1
  echo "A listagem de $1 foi executada!"
}

for dir in $1 $2 $3 $4 $5 $6 $7 $8 $9 do
lista_diretorio $dir
done


#ls $1
ls $2
ls $3
ls $4
ls $5
ls $6
ls $7#

# teste_funcao /bin /sbin /usr /tmp

------------------------
vim 
#!/bin/sh
# Este programa é um teste de funçao 
lista_diretorios() {
  ls $1
  echo "A listagem de $1 foi executada!"
}

for dir in $1 $2 $3 $4 $5 $6 $7 $8 $9 do
lista_diretorio $dir
done


#ls $1
ls $2
ls $3
ls $4
ls $5
ls $6
ls $7#

# teste_funcao /bin /sbin /usr /tmp


No sh, não existe função.
