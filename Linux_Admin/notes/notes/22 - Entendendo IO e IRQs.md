---
title: 22 - Entendendo IO e IRQs
tags: [Import-255a]
created: '2021-06-13T12:49:51.049Z'
modified: '2021-06-13T12:51:47.971Z'
---

---
title: 22 - Entendendo IO e IRQs
created: '2021-04-08T21:20:44.234Z'
modified: '2021-04-08T21:22:14.251Z'
---

# 22 - Entendendo IO e IRQs

/proc 

cat interrupts | less --> vê as interrupções dos hardwares 

cat ioports | less --> mostra a forma como é feito o envio e recebimento de dados no dispositivo 

cat dma |less --> acesso direto a memória
