---
title: 142 - O Crontab
tags: [Import-255a]
created: '2021-06-13T12:49:51.037Z'
modified: '2021-06-13T12:52:04.245Z'
---

---
title: 142 - O Crontab
created: '2021-05-20T20:15:18.047Z'
modified: '2021-05-20T20:25:49.327Z'
---

# 142 - O Crontab

crontab --> cria as tarefas

crontab -l --> mostra as tarefas pro usuário corrente

crontab -l -u usuario --> mostra as tarefas do usuario

crontab -e --> coloca as tarefas

abre um vim na primeira vez

#MINUTOS - HORAS - DIAS - MES - DIA DA SEMANA - COMANDO 

- minutos --> 0-59
- Horas 0 a 23
- Dias 1-31
- Mes 1-12
- Dia da semana 0-7 ( DOMINGO É 0 OU 7)

36 19 4 2 * touch /tmp/criado

* qualquer dia, hora...

*/5 19 4 2 * touch /tmpp/criado --> a cada 5 minutos 


vim /etc/cron --> configurações de tarefas
