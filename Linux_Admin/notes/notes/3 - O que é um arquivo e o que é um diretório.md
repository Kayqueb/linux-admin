---
title: 3 - O que é um arquivo e o que é um diretório
tags: [Import-255a]
created: '2021-06-13T12:49:51.049Z'
modified: '2021-06-13T12:51:46.089Z'
---

---
title: 3 - O que é um arquivo e o que é um diretório
created: '2021-01-13T23:02:15.546Z'
modified: '2021-01-13T23:08:19.991Z'
---

# 3 - O que é um arquivo e o que é um diretório

diretório --> local onde guarda arquivos e diretórios

d(diretório)-x-r
-(arquivo)x-r-w

cd usr/lib/systemd --> caminho absoluto

cd system -- caminho relativo (tem q estar no mesmo diretório)
