---
title: 35 - Administração de Usuários - Outros comandos
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:48.872Z'
---

---
title: 35 - Administração de Usuários - Outros comandos
created: '2021-04-09T15:04:58.727Z'
modified: '2021-04-09T15:09:31.383Z'
---

# 35 - Administração de Usuários - Outros comandos 

groupmod --> para mudar informações do grupo

lastlog --> mostra os ultimos usuários que logaram no sistema

last --> mostra a listagem de usuários que sairam e entraram no sistema

cat /var/log/lastlog --> local onde ele busca as informações

users --> mostra os ultimos usuários logados

groups --> mostra os ultimos grupos logados
