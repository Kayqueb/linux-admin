---
title: 46 - Redirecionadores - STDOUT E STDEER
tags: [Import-255a]
created: '2021-06-13T12:49:51.049Z'
modified: '2021-06-13T12:51:50.335Z'
---

---
title: 46 - Redirecionadores - STDOUT E STDEER
created: '2021-04-13T00:55:11.373Z'
modified: '2021-04-13T01:01:39.303Z'
---

# 46 - Redirecionadores - STDOUT E STDEER

ls fogete opa.txt. > saida_do_ls_maroto.txt 

Ele vai pegar o erro e jogar dentro do opa.txt e o saida_do_ls_maroto vai receber o nome do arquivo opa.txt




ls fogete opa.txt. > saida_do_ls_maroto.txt 2>&1

Ele vai pegar o erro e vai jogar pra saída padrão do stdout. Mas o stdout virou o arquivo saida_do_ls_maroto.txt

2>&1 --> ele avisa que vai enviar o erro pra um file descriptor, no caso o 1.
