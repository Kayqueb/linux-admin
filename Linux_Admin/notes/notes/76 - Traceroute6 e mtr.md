---
title: 76 - Traceroute6 e mtr
tags: [Import-255a]
created: '2021-06-13T12:49:51.053Z'
modified: '2021-06-13T12:51:53.643Z'
---

---
title: 76 - Traceroute6 e mtr
created: '2021-04-16T13:34:59.850Z'
modified: '2021-04-16T13:37:40.350Z'
---

# 76 - Traceroute6 e mtr

traceroute6 www.google.com --> utilizado pra ipv6

mtr -6 wwww.google.com --> utilizado pra ipv6

Para verificar perdas é melhor usar o traceroute
