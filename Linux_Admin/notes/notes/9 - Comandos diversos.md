---
title: 9 - Comandos diversos
tags: [Import-255a]
created: '2021-06-13T12:49:51.049Z'
modified: '2021-06-13T12:51:46.494Z'
---

---
title: 9 - Comandos diversos
created: '2021-02-03T22:47:15.694Z'
modified: '2021-03-18T20:37:50.267Z'
---

# 9 - Comandos diversos 

#ls-la --> salva o comando como comentário, bom pra salvar comandos grandes 

date --> exibe a data e hora local
date -u --> exibe o GMT ou UTC (zonas)
date -s 08:43 --> ajuste de hora, mas precisa ser root
hwclock --systohc --> salva a atualização do horário na placa mãe
Obs: O Linux salva automaticamente ao desligar ou reinicar
date +%d --> exibe o dia
y% --> exibe o ano
m% exibe o mês
ex: date +"%d-y%"


df --> mostra o espaço livre de cada partição montada no sistema
df -h --> formata em megas,gigas..
df -H --> blocos em tamanho comercial
df -l --> exibe sistemas de arquivos montados locais
df -m --> montado em megas
df -a --> detalhamento do filesystem
df -i --> mostra todos os inouds do disco
df -t --> mostra o sistema de arquivos formatado


ln --> cria um link de um arquivo para um diretório ou para outro arquivo
ln /bin/ls ls--> cria um hardlink para arquivo
ln -s /bin/ls ls-link --> cria um link simbolico apontando para um diretório


find --> lista todos os arquivos e diretórios do diretório atual.
find . -name ls --> localiza tudo que termina com ls
find . -name mc --> localiza binarios e pastas com mc
find . -type d -name mc --> localiza apenas pastas com mc
find . -type f -name mc --> localiza apenas arquivos
find . -type /var -type f -name mc --> localiza dentro da pasta var
find /usr -maxdepth 2 -type f -name mc --> trouxe pastas que tem dois níveis na estrutura de arquivos Ex:  /usr/bin/mc
find /usr -mindepth 3 -maxdepth 4 -type f -name mc --> traz pasta com no minimo 3 na estrutura e com no maximo 4. EX: /usr/share/lintian/overrides/mc
find /etc -mtime -1 --> traz todos os arquivos que foram modificados um dia atrás

OBS: REVISAR 

find /etc -amin -10 --> arquivos acessados nos ultimos 10 min
find /etc -min -1 --> todos acessados no ultimo minuto
find /etc -cmin -1 --> criados no ultimo minuto
find /etc -cmin -ctime -1 --> arquivos ou pastas que  foram criados a menos de 1 dia atrás
find /etc -cmin -ctime +1 --> mais de um dia atrás

find /usr -mount -name ls --> faz pesquisa dentro do ponto montado 
find /usr -gid 1000 --> pesquisa por grupos
find /usr -uid 1000 --> pesquisa por usuarioss 
find /home -user kayque --> pesquisa todos os arquivos e grupos do usuario
find /usr -links 3 --> lista todos os links com 3 referencias 
find /usr -size -1000 --> todos os arquivos com tamanho < que 1000
find /usr -size +1000 --> todos os arquivos com tamanho > que 1000
find /dev -type --> pesquisar por tipos

OBS: CAI NA LPI
free --> mostra a memoria disponível RAM e a SWAP
free --kilo --> kilobite
free --mega --> megabites
free --kibi --> mostra em kibibites
free --mebi --> mostra em mebibites
free --gibi --> mostra em gibibites

free --mega -s 1 --> mostra em cada 1 segundo


grep --> pesquisa por expressões
grep 'root' /etc/passwd --> exibe as linhas que possuem a palavra root dentro do arquivo
grep -v 'www-data' /etc/passwd --> exibe todas as linhas que não tem a palavra especificada 
grep -f /tmp/expressao /etc/passwd --> le o arquivo e exibe
grep -i 'WWW-data' --> ignora maius e minusc
grep -iE '^www-data.*nologin$' /etc/passwd --> pesquisa por expressoes regulares. 
grep -iF '.*' /etc/passwd --> pesquisa mesmo com caracteres especiais 
grep -r --> exibe a pesquisa de forma recursiva 
ex: grep -ri 'Foca02' . --> traz todos os nomes de arquivos com o nome 'Foca02'
grep -rih --> oculta todos os arquivos com 'Foca02'
grep -ril --> lista todos os arquivos
grep -rin --> exibe com o numero das linhas


head --> mostra as 10 primeiras linhas do arquivo
head -n 3 passwd --> mostra as 3 primeiras linhas
head -c 10 passwd --> mostra os 10 primeiros bites do arquivo


nl /etc/passwd --> exibe o numero de linhas(Mais customizado)


more --> abre o arquivo e pausa ele pagina a pagina (obs: só rola para baixo)

less --> abre o arquivo e pausa de pagina a pagina (obs: sobe,desce e vai pro lado e pro outro)

sort lista.txt --> ordena por ordem numerica e ordem alfabética 

tail --> permite visualizar o final do arquivo(10 ultimas)
tail -n 4 /etc/passwd --> exibe as 4 ultimas linhas

time --> tempo de execução de um comando 
touch --> cria arquivo com tamanho vazio
touch -t 10120815 /tmp/arquivo --> modifica a data de criação do arquivo para 10/12 às 08:15
touch -a -t 10120815 /tmp/arquivo --> modifica a hora de acesso no arquivo

uptime --> tempo de boot da maquina
dmesg --> mostra as mensagens exibidas em buffer
dmesg -t | grep enp0s3 --> mostra as mensagens exibidas no adaptador 
dmesg --color-never --> exibe em preto e branco
dmesg --color-auto --> exibe em cores
dmesg --w --> exibe ao vivo
dmesg --x --> exibe as mensagens em nível e de forma mais legível 
dmesg --c --> limpa as mensagens do buffer do kernel 

mesg  --> recebe ou não mensagem do comando talk

echo --> exibe uma mensagem na tela
echo -n "teste" --> não faz a quebra de linha 
echo -e "teste " -> exibe caracteres especiais 

su --> eleva a permissão para usuário root. Utiliza quando você conhece a senha de root
sudo --> só pra quem não conhece

su - --> elimina todas as variaveis de ambiente
/bin/su -  --> força o sistema a subir de forma adequada 

sync --> grava os buffers do kernel no disco

uname -a --> exibe todos os dados do linux e kernel
uname -s --> exibe o kernel 
uname -n --> nome da maquina na rede 
uname -r --> versão atual do kernel
uname -v --> data do kernel foi compilado 
uname -m --> arquitetura 

reboot --> apenas root
/bin/su - root  e depois reboot 
systemctl reboot --> reinicia a máquina
reboot -f --> ele reinicia de uma vez só (n salva nada)
shutdown -r now --> reinicia no ato
echo b>/proc/sysrq-trigger --> reinicia no ato. Mas usado quando o disco travou e está no kernel panic
halt --> desliga a máquina
systemctl halt
shutdown -h now 
echo o >/proc/sysrq-trigger --> ultimo recurso para desligar
shutdown -h 09:50 --> agenda para desligar com notificação
shutdown -c --> cancela
shutdown -h +10 --> desliga daqui a 10 min

wc /etc/passwd --> exibe o tamanho do arquivo, numero de linhas e palavras
wc -l --> apenas o numero de linhas
wc -c --> numero de bites
wc -w --> numero de palavras

seq --> imprime uma sequencia de numeros 
seq 10 --> enumera de 1 a 10
seq 2 2 10 --> começa com 2 e pula de 2 em 2 até 10
seq -w --> alinha os numeros

REVISAR
chattr --> altera atributos do sistema de arquivos do linux
chattr +i --> imutável. Ele protege contra qualquer alteração
chattr -i --> torna mutável
lsattr --> lista os atributos

chattr +a teste --> só posso fazer um appending
Ex: echo "Linha 1" >> teste3
echo "Linha 2" >> teste3 (adiciona a mensagem no final do arquivo)
echo "Linha 4" > teste3(erro)


cut -d ":" -f 1 /etc/passwd --> pega o primeiro campo do arquivo
cut -b 1-4 /etc/passwd --> pega 1 ao 4 bit do arquivo

cmp teste teste-copia --> compara os arquivos e retorna os bites diferentes
diff -u teste teste-copia --> retorna qual a diferença na comparação de forma mais clara

whereis --> permite localizar o arquivo que contem a página de manual

which --> permite encontrar o binário do sistema e onde está localizado
