---
title: 39 - O comando CHOWN e o CHGRP
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:49.179Z'
---

---
title: 39 - O comando CHOWN e o CHGRP
created: '2021-04-09T21:44:22.538Z'
modified: '2021-04-09T21:55:52.726Z'
---

# 39 - O comando CHOWN e o CHGRP

sudo chown giropops teste --> novo dono de teste

sudo chown jeferson:users teste --> usuario dono é o jeferson e o grupo dono é o users

sudo chown :jeferson teste --> alterou grupo para jeferson

sudo chown -R giropops:users teste_dir/ --> o dono é o giropops e o grupo é o users  

sudo chgrp jeferson teste_dir/ --> o jeferson será dono do dir teste_dir/
