---
title: '113 - Kernel - insmod, rmmod e modprobe'
tags: [Import-255a]
created: '2021-06-13T12:49:51.037Z'
modified: '2021-06-13T12:51:59.181Z'
---

---
title: '113 - Kernel - insmod, rmmod e modprobe'
created: '2021-05-06T23:54:49.870Z'
modified: '2021-05-07T20:52:52.019Z'
---

# 113 - Kernel - insmod, rmmod e modprobe

kmod --> ferramenta que gerencia remoção e adição de modulos em funcionamento. 

rmod e1000 --> você remove o modulo

lsmod --> lista 

lsmod /lib/modules/pasta da versão/

insmod /lib/modules/pasta/kernel/drivers/

insmod /lib/modules/pasta/kernel/drivers/qualmodulo/arquivo.ko

modprobe cdrom --> carrega junto com as dependencias. arquivo.dep

modprobe -r --remove-dependencies cdrom 

dmesg --> mostra o modulos
