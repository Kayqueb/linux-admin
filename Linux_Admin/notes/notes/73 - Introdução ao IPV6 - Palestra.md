---
title: 73 - Introdução ao IPV6 - Palestra
tags: [Import-255a]
created: '2021-06-13T12:49:51.053Z'
modified: '2021-06-13T12:51:53.345Z'
---

---
title: 73 - Introdução ao IPV6 - Palestra
created: '2021-04-16T12:53:21.409Z'
modified: '2021-04-16T13:17:01.694Z'
---

# 73 - Introdução ao IPV6 - Palestra

IPV6 --> Novo protocolo de roteamento de comunicação entre computadores. 

  - Numero maior de enedereços 
  - Design mais moderno(Largura de janela variável, mais rápido)
  - Aproveitamento do endereçamento - 340 UNDECILHÕES DE ENDEREÇOS
  


COMPARAÇÃO COM IPV4


.Permite a co-existencia com o IPV4 
.Aproveitamento de endereços(Não tem broadcast/network, é usado o multicast)
.Endereços hexadecimais 0 a f
.Um/126 tem 3 endereços. Equivalente ao /30
.Não é informada uma máscara de rede para fazer a operação de AND igual no IPV4.   Notação de bit count foi mantida
.Linux suporta desde 2004
.Melhor experiência de navegação
.Serviços como PPPoE ou RadVD precisam de blocos /64 por  conta de montagem de endereços usando MAC address
.Um máscara /32 é MT IP
.Um /32 pode ter 65536 sub-redes /48


ENDEREÇOS RESERVADOS EQUIVALENTES AO IPV4

 - ::1/128 - Loopback
 - ::/128 - nao especificado
 - ::ffff:0:0/96 - mapeamento ipv4
 - 64:ff9b::/96 - tradução ipv4-ipv6
 - 2001:db8::/32 - usado em doc
 - fc00::/7 - rede privada
 - fe80::/10 - endereço local



COMO COMPARTILHAR INTERNET IPV6


 -Configurar o ipv6 local da sua rede usando a faixa privada fc00/07
 -Configurar a distrib de ipv6 no servidor usando RadVD ou ipv6
 - Adicionar a rota: para os serviços internos:
 - ip -6 route add 2000::/3 via 2804:23a:35c:100:a::2
 (suponha q o ip é o recebido do provedor remoto)
 - Habilitar ipforward no IPV6(1)
 - cat /proc/sys/net/ipv6/conf/all/forwarding
 - Habilitar nat: ip6tables -t nat -A POSTROUTING -s
      fc00:f0ca:f0ca::0/64 -o ens3p1 -j MASQUERADE
