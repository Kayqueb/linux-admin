---
title: 176 - Inspecionando containers e Configurações
tags: [Import-255a]
created: '2021-06-13T12:49:51.025Z'
modified: '2021-06-13T12:52:09.756Z'
---

---
title: 176 - Inspecionando containers e Configurações
created: '2021-05-24T20:33:52.807Z'
modified: '2021-05-24T20:35:30.022Z'
---

# 176 - Inspecionando containers e Configurações 

docker container inspect nginx --> dados de configurações do container

docker image inspect nginx --> dados da imagem
