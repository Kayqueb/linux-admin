---
title: 133 - Customizando o GRUB
tags: [Import-255a]
created: '2021-06-13T12:49:51.033Z'
modified: '2021-06-13T12:52:03.291Z'
---

---
title: 133 - Customizando o GRUB
created: '2021-05-18T22:44:17.910Z'
modified: '2021-05-18T22:49:23.783Z'
---

# 133 - Customizando o GRUB

/etc/default/grub --> local de edição


GRUB_DEFAULT=0 --> S0
_TIMEOUT=5 --> Tempo pra iniciar
_DISTRIBUTOR= --> Distribuição
_CMDLINE_LINUX_DEFAULT ="quiet"
_CMDLINE_LINUX=""

update-grub --> gera um grubconfigurationfile 

reboot
