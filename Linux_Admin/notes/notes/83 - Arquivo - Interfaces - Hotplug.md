---
title: 83 - Arquivo - Interfaces - Hotplug
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:54.742Z'
---

---
title: 83 - Arquivo - Interfaces - Hotplug
created: '2021-04-17T13:42:59.128Z'
modified: '2021-04-17T13:54:05.664Z'
---

# 83 - Arquivo - Interfaces - Hotplug 

cd /etc/network --> padrão 

vim interfaces

auto lo --> qual interface que quer subir

allow-hotplug enp0s3 --> só subirá a rede se tiver um link funcional. Usado para um link redundante. Sobe automaticamente. E sai automaticamente


up echo "subiu" >> /tmp/conexoes.log --> envia uma mensagem que o link subiu
down echo "caiu" >> /tmp/conexoes.log  --> mostra que caiu


iface enp0s3 inet static --> metoda para datacenter de menor porte. Ip statico

ifup enp0s3 --> só obtém o ip se for dhcp
ifdown enp0s3 


dhclient --> gerencia a mudança de ip

dhclient enp0s3 --> força a mudança
