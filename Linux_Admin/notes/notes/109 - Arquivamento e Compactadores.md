---
title: 109 - Arquivamento e Compactadores
tags: [Import-255a]
created: '2021-06-13T12:49:51.041Z'
modified: '2021-06-13T12:51:58.393Z'
---

---
title: 109 - Arquivamento e Compactadores
created: '2021-05-06T22:51:05.344Z'
modified: '2021-05-06T23:36:09.832Z'
---

# 109 - Arquivamento e Compactadores

tar --> juntas os arquivos

tar -cvf /tmp/arquivo.tar  /usr/bin --> junta todos os arquivos um atrás do outro

xz --> usado para links mais leves. Compacta rápido com processamento

tar -cvf - /usr/bin | gzin -c  > /tmp/arquivo.tar.gz 

gzip -d arquivo.tar.gz --> descompacta

tar -xvf arquivo.tar --> separa os arquivos

tar -cvzf /tmp/arquivo.tar.gz /usr/bin --> arquiva e compacta gzip

tar -cvJf /tmp/arquivo.tar.bz2 /usr/bin --> arquiva e compacta com bzip

tar -cvjf /tmp/arquivo.tar.bz2 /usr/bin --> compacta como bzip

tar -cvJf /tmp/arquivo.tar.xz /usr/bin --> arquiva e compacta com xz. Demora muito mais
