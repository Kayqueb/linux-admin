---
title: 29 - Administração de Usuários - Adicionando grupos
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:48.433Z'
---

---
title: 29 - Administração de Usuários - Adicionando grupos
created: '2021-04-09T14:03:22.057Z'
modified: '2021-04-09T14:13:01.713Z'
---

# 29 - Administração de Usuários - Adicionando grupos

groupadd girous --> adiciona o grupo, não segue ordem

cat /etc/group

vboxusers:x:136: -->
nome do grupo:senha do grupo:gid

addgroup catota --> adiciona o grupo, seguindo o grupo de usuários em ordem

adduser --group cabecao --> adiciona um grupo, seguindo o grupo de usuários em ordem

newgrp - strigus -->  adiciona o seu usuário a um grupo durante a sessão
