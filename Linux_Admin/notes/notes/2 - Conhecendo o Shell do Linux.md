---
title: 2 - Conhecendo o Shell do Linux
tags: [Import-255a]
created: '2021-06-13T12:49:51.049Z'
modified: '2021-06-13T12:51:45.991Z'
---

---
title: 2 - Conhecendo o Shell do Linux
created: '2021-01-13T22:22:33.221Z'
modified: '2021-01-13T22:42:50.616Z'
---

# 2 - Conhecendo o Shell do Linux

cd -   --> volta para o último diretório que você estava 

cd ~   --> vai para um diretório a frente

ls --> lista arquivos
ls -a --> All - lista todos os arquivos(ocultos também .arquivo)
ls -al --> lista todos os arquivos em bites(4096)
ls -lha  --> lista todos os arquivos em mega,giga, peta... 

ctrl+l --> clear

history --> histórico de comandos
cat /etc/shells --> exibe os shells existentes na máquina

ctrl+d --> sair(exit)

. --> diretório atual
..  --> diretório anterior
