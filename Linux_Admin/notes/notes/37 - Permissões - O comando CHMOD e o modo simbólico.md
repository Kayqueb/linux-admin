---
title: 37 - Permissões - O comando CHMOD e o modo simbólico
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:49.027Z'
---

---
title: 37 - Permissões - O comando CHMOD e o modo simbólico
created: '2021-04-09T18:34:44.843Z'
modified: '2021-04-09T20:05:58.126Z'
---

# 37 - Permissões - O comando CHMOD e o modo simbólico

- Modo Simbólico -

chmod u+x teste --> fala pro dono(u) adicionar a permissão de execução

chmod g-w teste --> remove do grupo a permissao de execucao

chmod  o+x teste --> adiciona em outros a permissão de execução

chmod a=rw teste --> todo mundo com a permissão igual a rw

chmod u=rw, g=rx, o=r teste -->
