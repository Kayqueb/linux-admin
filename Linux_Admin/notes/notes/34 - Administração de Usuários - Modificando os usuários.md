---
title: 34 - Administração de Usuários - Modificando os usuários
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:48.802Z'
---

---
title: 34 - Administração de Usuários - Modificando os usuários
created: '2021-04-09T14:50:44.207Z'
modified: '2021-04-09T15:04:30.860Z'
---

# 34 - Administração de Usuários - Modificando os usuários 

usermod -u 5555 -g 0 "giropops strigus girus" -d /home/girus -S /bin/zsh girus -->

vai modificar o uid, o gid e colocar um comentário "", vai definir o /home do usuário e o shel utilizado

obs: precisa criar o diretório do /home, pois se n houver, ele não cria automaticamente 

usermod -l lua girus --> muda o nome do usuário girus pra lua

usermod -m -d /home/strigus_novo strigus --> muda o diretório home antigo e moveu para o novo

chfn --help --> change full name 

chfn -f jeferson -h 333333 lua --> jeferson vira o full name e o 333333 o telefone 

chfn lua --> menu pra colocar as informações do usuário

chsh lua --> menu para mudar o shell
