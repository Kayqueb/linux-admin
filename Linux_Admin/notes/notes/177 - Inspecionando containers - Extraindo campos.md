---
title: 177 - Inspecionando containers - Extraindo campos
tags: [Import-255a]
created: '2021-06-13T12:49:51.029Z'
modified: '2021-06-13T12:52:09.882Z'
---

---
title: 177 - Inspecionando containers - Extraindo campos
created: '2021-05-24T20:36:33.110Z'
modified: '2021-05-24T20:37:24.673Z'
---

# 177 - Inspecionando containers - Extraindo campos

docker container inspect  --format "{{.NetworkSettings.IPAddress}}" nginx
