---
title: 14 - Opções de Montagem - Partições
tags: [Import-255a]
created: '2021-06-13T12:49:51.049Z'
modified: '2021-06-13T12:51:47.493Z'
---

---
title: 14 - Opções de Montagem - Partições
created: '2021-03-26T19:24:27.249Z'
modified: '2021-03-27T02:20:01.657Z'
---

# 14 - Opções de Montagem - Partições

dump2fs /dev/sda6 | less --> dados internos do disco

Filesystem volume name: --> label

UUID --> identificaçao de um dispositivo de bloco

blkid -s UUID /dev/sda6 --> identificaçao de um disp de bloco 

mcedit -b /etc/fstab
UIID =numero do UUID /var ext4  --> montagem com esse UUID(padrão utilizado)

tune2fs - L part_var /dev/sda6 --> define o label

tune2fs -i 180 /dev/sda1 --> determina uma verificação no disco a cada 180 dias

tune2fs -m 1 /dev/sda6 --> define 1% dos blocos para manutenção
(Partições acima de 1Tera, pode reservar 0.1)

tune2fs -O ^has_journal /dev/sdb3 --> desabilita uma feature

tune2fs -O has_journal /dev/sdb3 --> ativa novamente o journal

mcedit -b /etc/
