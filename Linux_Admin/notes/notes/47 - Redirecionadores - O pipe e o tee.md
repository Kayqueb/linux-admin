---
title: 47 - Redirecionadores - O pipe e o tee
tags: [Import-255a]
created: '2021-06-13T12:49:51.053Z'
modified: '2021-06-13T12:51:50.418Z'
---

---
title: 47 - Redirecionadores - O pipe e o tee
created: '2021-04-13T01:02:20.708Z'
modified: '2021-04-13T01:04:54.368Z'
---

# 47 - Redirecionadores - O pipe e o tee

O pipe pega o conteudo do comando anterior e joga pro comando a ser processado. 

ls | tee arq_novo.txt 

pega o ls e cria o "arq_novo.txt"

O tee pega o conteudo do arquivo e exibe na tela
