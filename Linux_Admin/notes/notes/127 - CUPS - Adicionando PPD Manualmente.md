---
title: 127 - CUPS - Adicionando PPD Manualmente
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:52:01.694Z'
---

---
title: 127 - CUPS - Adicionando PPD Manualmente
created: '2021-05-14T19:11:21.975Z'
modified: '2021-05-14T19:14:28.448Z'
---

# 127 - CUPS - Adicionando PPD Manualmente

lpadmin --> interface mais ativa

lpadmin -x lp --> removeu a impressora lp

lpstat -p PDF -l --> status 

lpadmin -p lp -E socket://ip -m Laserjet.ppd --> adiciona a impressora
