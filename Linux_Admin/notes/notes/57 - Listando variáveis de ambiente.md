---
title: 57 - Listando variáveis de ambiente
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:51.444Z'
---

---
title: 57 - Listando variáveis de ambiente
created: '2021-04-15T12:12:31.048Z'
modified: '2021-04-15T13:00:54.583Z'
---

# 57 - Listando variáveis de ambiente 

O que é variável de ambiente?

Coloca um nome pra um campo que vou acessar na memória a qualquer momento. 



$PATH --> lista de diretórios que contém binários

echo --> printa uma mensagem

echo $PATH

set --> mostra todas as variáveis que existem, shells, funções do shell

env --> imprime as variáveis de ambiente atual

printenv --> mostra as variáveis de ambiente

nome da variável(maiuscula)=valor dela
