---
title: 36 - Permissões - O que é issoʔ
tags: [Import-255a]
created: '2021-06-13T12:49:51.053Z'
modified: '2021-06-13T12:51:48.954Z'
---

---
title: 36 - Permissões - O que é isso?
created: '2021-04-09T15:44:10.291Z'
modified: '2021-04-12T23:10:47.789Z'
---

# 36 - Permissões - O que é isso?

SIMBÓLICO
-   rw-   rw-  r--

tipo do arquivo  
permissoes do dono(arquivo)
permissoes do grupo(dono do arquivo)
outros(quem não é dono e nem do grupo)

- Tipos de arquivos -

- --> arquivo de texto
l --> link simbolico
b --> dispositivo de bloco
c --> dispositivo de caractere 
s --> socket
d --> diretório

LETRA 

- --> ausencia da permissao
r --> read (permissao de leitura)
w --> write (permissao de escrita e apagar)
x --> execution (permissao de execuçao) 
t --> stick bit
S --> SGID
S --> SUID

OCTAL 

- --> 0
r --> 4
w --> 2
x --> 1
t --> 1
S --> 2 (SGID)
S --> 4 (SUID)

SOMA
- rw-   rw-  r--
  420   420  400

   6     6    4

421 4 2 4

COMANDOS

chmod --> modifica as permissoes

chown --> modifica o dono/grupo do arquivo/dir

chgrp --> modifica o grupo do arquivo/dir

umask --> define a permissão padrão de arquivos/ diretorios


QUEM? 

- --> u (dono do arquivo)
r --> g (grupo dono)
w --> o (outros)
x --> a (all)
