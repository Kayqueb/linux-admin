---
title: 7 - Comandos de Manipulação de Diretórios
tags: [Import-255a]
created: '2021-06-13T12:49:51.049Z'
modified: '2021-06-13T12:51:46.391Z'
---

---
title: 7 - Comandos de Manipulação de Diretórios
created: '2021-01-22T18:37:37.144Z'
modified: '2021-02-03T22:13:10.037Z'
---

# 7 - Comandos de Manipulação de Diretórios

ls --> lista os arquivos

-a --> arquivos, diretórios +ocultos e diretorios seguintes e anterior (. , ..)

-A --> Lista diretorios e arquivos ocultos
-B --> Lista arquivos sem ~no final. (arquivos de backup)

--color=auto --> Colore arquivo dentro de um terminal que suporte cores

-d --> lista o nome da pasta
-l --> lista detalhes
-f --> lista por ordem de criação
-F --> coloca um separador pra identificar os arquivs. Ex: pasta/ 
-n --> lista em formato numerico o proprietário
-L --> lista sem os apontamentos
-o --> proprietário do diretório
-g --> o grupo do diretório
-t --> lista por data recente
-latr --> reverte a listagem os mais novos modificados primeiro
-lac --> c-time para listar
-lax --> lista pela extensão
-laR -- lista de forma recursiva (separado)


mkdir -p Diretorio15/Diretorio15_1 --> Permite que crie uma estrutura de pastas sem que exista alguma criada

tree --> vê a árvore de diretórios 
tree -A

rmdir -p caminho/caminho/  --> apaga diretórios se estiverem vazios

cat -n nomedoarquivo --> exibe o numero da linha na frente do arquivo
cat -s nomedoarquivo --> exibe o arquivo removendo as linhas em branco
cat -b nomedoarquivo --> exibe o numero da linha do arquivo, eliminando as linhas em branco

cat -E --> exibe um $ no final da linha. Usado pra localizar o fim da linha

zcat teste.gz --> visualiza o arquivo .gz
xzcat teste.gz --> visualiza e extrai

tac --> exibe o arquivo de forma invertida. Ultima linha será a primeira

rm --> remove um arquivo 
rm -r --> remove com subdiretórios com perguntas
rm -rf --> remove diretório forçadamente e sem perguntar
rm -i --> remove diretório perguntando
rm -f a* --> apaga todos os arquivos com a letra
rm -- -  --> apaga um arquivo que seja iniciado por um traço ou o próprio traço. Pode remover também arquivos com * ?

cp [origem] [destino] -- copia o arquivo
ex: cp aaa eee 
cp /bin/ls  arquivo

cp -R --> copia dispositivos especiais
cp -u --> copia arquivos somente se forem atualizados na origem
cp -x -->  Específica se você vai copiar pastas com arquivos em outros sistemas de arquivos.(PARTIÇÃO SEPARADA)
cp -p --> preserva atributos do arquivo. permissões e donos do arquivo
cp -a --> = dpR --> preserva o máximo o sistema de arquivos


mv --> move o arquivo para outro local, mas apaga a origem do arquivo. obs: mesmo sistema de arquivos.

mv -i aa destino --> pergunta se pode mover o arquivo

mv -i eee aaa --> você consegue renomear
mv -f --> move sem perguntar
mv -
