---
title: 54 - Gerenciadores de Pacotes - RPM
tags: [Import-255a]
created: '2021-06-13T12:49:51.045Z'
modified: '2021-06-13T12:51:51.155Z'
---

---
title: 54 - Gerenciadores de Pacotes - RPM
created: '2021-04-14T22:49:17.278Z'
modified: '2021-04-14T22:55:39.175Z'
---

# 54 - Gerenciadores de Pacotes - RPM

wget --> faz o download 

rpm -ivh --> i de install, v de verboso e h de hash(porcentagem)

rpm -ivh pacote.rpm --> pra instalar

rpm -Uvh pacote.rpm --> pra fazer o update

rpm -ev pacote --> remove o pacote

rpm -qa --> pega todos os pacotes instalados 

rpm -ql wget--> pega todos os arquivos relacionados 

rpm -qi --> informações do app instalado

rpm -qc wget --> mostra o arquivo de configuração

rpm -qa --last --> mostra os softwares instalados por ultimo
