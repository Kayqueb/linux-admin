---
title: 90 - Segurança - Securetty
tags: [Import-255a]
created: '2021-06-13T12:49:51.053Z'
modified: '2021-06-13T12:51:55.356Z'
---

---
title: 90 - Segurança - Securetty
created: '2021-05-03T22:37:12.691Z'
modified: '2021-05-03T22:45:04.150Z'
---

# 90 - Segurança - Securetty

cat /etc/securetty | less

w --> exibe os usuários conectados da máquina

/0 --> conectou remotamente

export TMOUT=10 --> desloga do sistema

tail -f /var/log/auth.log --> logs de autenticação ou erros de logins
