---
title: 61 - Arquivos para customizar o shell
tags: [Import-255a]
created: '2021-06-13T12:49:51.041Z'
modified: '2021-06-13T12:51:52.365Z'
---

---
title: 61 - Arquivos para customizar o shell
created: '2021-04-15T13:33:16.052Z'
modified: '2021-04-15T13:57:38.008Z'
---

# 61 - Arquivos para customizar o shell 

ls -lha .bash --> arquivos pra customizar o bash
 .bash_history --> historico
 .bash_logout --> arquivo do logout, onde posso fazer alterações pra realizar funções ao deslogar
 .bash_login --> arquivo de login, onde posso fazer alterações ao iniciar o linux
 .bash_rc --> onde faço as edições no bash. Local para perpetuar alterações. É lido após o /etc/profile
tudo no diretório /home
bash--> para entrar em vigor as alterações do bash
source --> vai reler o arquivo

vim /etc/profile --> É o primeiro a ser lido. O mesmo que um .bash_rc, mas de forma definitiva pra todos os usuário. É interessante criar um .profile apenas pro usuário.

PS1="\u(usuario)@\h(hostname):\w$(/home)" --> ele traz o usuario, hostname e o tipo de usuário


vim /etc/motd ou mod --> edita a mensagem do dia(ou conectar via ssh)
