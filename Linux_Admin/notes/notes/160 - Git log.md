---
title: 160 - Git log
tags: [Import-255a]
created: '2021-06-13T12:49:51.025Z'
modified: '2021-06-13T12:52:07.070Z'
---

---
title: 160 - Git log
created: '2021-05-20T21:59:11.196Z'
modified: '2021-05-20T22:03:08.116Z'
---

# 160 - Git log

git log meuteste --> traz os logs no arquivo e quem fez alterações

- Se alguém editar no próprio site do git

cat meuteste --> mostra se houve alteração

git pull --> pra atualizar

git log --help --> manual

git log -n 2 meuteste --> mostra os últimos dois commits
