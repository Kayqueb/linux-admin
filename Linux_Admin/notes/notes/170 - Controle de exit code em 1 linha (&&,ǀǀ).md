---
title: '170 - Controle de exit code em 1 linha (&&,ǀǀ)'
tags: [Import-255a]
created: '2021-06-11T22:08:47.096Z'
modified: '2021-06-13T12:52:08.049Z'
---

---
title: '170 - Controle de exit code em 1 linha (&&,||)'
created: '2021-05-24T18:36:32.911Z'
modified: '2021-05-24T18:49:36.615Z'
---

# 170 - Controle de exit code em 1 linha (&&,||)

true --> exibe sempre o exit code 0

false --> exit code diferente de 0

true && pwd --> se o último comando for verdadeiro, exibe o pwd

if $(false) ; then pwd; fi --> mesma ação q o &&

if `true`; then pwd; fi --> ele também exibe um comando externo

false || pwd --> executa o próximo comando se o anterior falhar
