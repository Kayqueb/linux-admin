---
title: 184 - Conceito de dados mutáveis e imutáveis
tags: [Import-255a]
created: '2021-06-13T12:49:51.021Z'
modified: '2021-06-13T12:52:11.503Z'
---

---
title: 184 - Conceito de dados mutáveis e imutáveis
created: '2021-05-24T20:55:06.377Z'
modified: '2021-05-24T20:59:24.605Z'
---

# 184 - Conceito de dados mutáveis e imutáveis

imutaveis --> que não serão alterados

mutaveis --> poderão ser mudados 

docker container run -d -v /tmp/www:/usr/local/apache2/htdocs -p 8080:80 --name apache httpd --> sobe um container com uma página
