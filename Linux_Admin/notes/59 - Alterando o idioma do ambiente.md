---
title: 59 - Alterando o idioma do ambiente
created: '2021-04-15T13:16:45.158Z'
modified: '2021-04-15T13:24:43.970Z'
---

# 59 - Alterando o idioma do ambiente 

cat /etc/locale.gen --> traz todos os arquivos de idioma

export LC_MESSAGES=pt_BR.UTF-8
export LC_ALL=pt_BR.UTF-8   --> todas as mensagens
export LC_LANG=pt_BR.UTF-8

Coloca as mensagens em português
