---
title: 5 - Desligando e reiniciando o Linux
created: '2021-01-20T04:08:21.492Z'
modified: '2021-01-20T04:13:20.033Z'
---

# 5 - Desligando e reiniciando o Linux

halt 
reboot
shutdown -h now -- desliga
shutdown -r now --> reinicia 
power off --> desliga
init 0 --> desliga
init 6 --> reinicia 

shutdown -h 18:00 --> programado 
shutdown -c --> retira a programaç~ao
shutdown +30 --> fica mais 30 min
shutdown +30 "Preciso desligar em 30 min" 
