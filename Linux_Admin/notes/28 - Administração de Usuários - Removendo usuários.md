---
title: 28 - Administração de Usuários - Removendo usuários
created: '2021-04-09T01:48:35.101Z'
modified: '2021-04-09T01:54:30.740Z'
---

# 28 - Administração de Usuários - Removendo usuários

deluser --remove-home girus --> remove o usuário e o diretório
userdel -r  girus --> remove o usuário e o mail spool
