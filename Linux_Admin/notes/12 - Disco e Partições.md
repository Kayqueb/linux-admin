---
title: 12 - Disco e Partições
created: '2021-03-24T01:10:06.417Z'
modified: '2021-04-07T19:35:52.086Z'
---

# 12 - Disco e Partições

fdisk /dev/sda --> exibe e seleciona o disco primario
o p n p 1 --> pra criar uma partição

+400M --> define o tamanho da partição

mkfs.ext3 /dev/sdb1 --> formata a partição
mkswap /dev/sdb2 --> formata a partição swap
wipefs -a /dev/sdb --> destroi todas as partições criadas

gdisk /dev/sdb --> detecta as partições
o --> cria uma partição GPT
n --> cria a partição 1 a 128

8300 --> Cria filesystem Linux
8200 --> Cria Linux Swap

b --> cria um backup do disco
/tmp/backup.diskgpt --> permite restaurar o disco

mkfs.vfat /dev/sdb2 --> formatar como fat
apt-get install dosfstools --> instalar

v --> verifica se deu algum problema na partição dos discos

x --> modo expert(estudar) 
d --> alinha os setores(muito usado em storage)

parted --> permite shell e gráfico

parted /dev/sdb --> inicia
help --> mostra os comandos
1 - passo 
mklabel msdos(gpt) --> tipo de partição
mkpart --> cria a partição(primaria ou extendida) 
set 1(numero da part) boot on --> definido como boot

ordem primary - extended - logical(swap)
sw --> swap
quit -- sai
unit s --> unidade de medida por setores
unit kb --> kibyte
rm --> remove a partição


cfdisk /dev/sdb --> modo gráfico
dos --> máximo 2tb de tamanho
gpt --> acima de 2tb até 1zb


Caso ao usar o mkswap receba a mensagem 'file has holes'? --> o arquivo de swap foi criado usando um arquivo copy-on-write, que não aloca espaço em disco no momento da criação


Para ganhar mais performance para listar os pontos de montagem? --> vá a fonte dando cat /proc/mounts











