---
title: 63 - Gerenciamento de Logs - O que é e como visualizar
created: '2021-04-15T14:48:46.135Z'
modified: '2021-04-15T21:21:27.942Z'
---

# 63 - Gerenciamento de Logs - O que é e como visualizar 

Log não é somente erros. É um registro de eventos!

cd /var/log --> diretório de logs default

  messages --> principal arquivo no debian


tail -f --> comando muito usado para ver os eventos 

Porta padrão rsyslog --> 514

Modulo do rsyslog pra habilitar recebimentos de logs remotos no udp --> imudp
