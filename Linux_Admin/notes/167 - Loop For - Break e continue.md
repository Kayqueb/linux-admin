---
title: 167 - Loop For - Break e continue
created: '2021-05-24T15:15:09.131Z'
modified: '2021-05-24T15:19:41.317Z'
---

# 167 - Loop For - Break e continue

#!/bin/bsh 
#ls
echo -n "Entre com o nome de usuario: "
read USUARIO

if ["$USER" != "$USUARIO"]; then
    echo "Nao esta rodando como $USUARIO"
    exit 1
fi 

if [-d "/tmp/copia"]; then
    mkdir /tmp/copia
fi

for VAR in $(seq 10 25); do
  if [ -d "/tmp/copia/dir$VAR"]; then
    echo "Diretorio /tmp/copia/dir$VAR existe, ignorando"
    if [ "$USER" = "guiafoca" ]; then
      echo " Rodando como guia foca"
      break
  else
    echo "não existe numero dir$VAR"
    mkdir /tmp/copia/dir$VAR #cria diretorios
  fi
done

echo " O for foi finalizado"

if [! -f "/tmp/copia/ls"]; then
    cp /bin/ls /tmp/copia
fi
exit 0

Obs; Quando  chegar no "rodando guia foca", ele quebra o loop.

----------------------------------

#!/bin/bsh 
#ls
echo -n "Entre com o nome de usuario: "
read USUARIO

if ["$USER" != "$USUARIO"]; then
    echo "Nao esta rodando como $USUARIO"
    exit 1
fi 

if [-d "/tmp/copia"]; then
    mkdir /tmp/copia
fi

for VAR in $(seq 10 25); do
  if [ -d "/tmp/copia/dir$VAR"]; then
    echo "Diretorio /tmp/copia/dir$VAR existe, ignorando"
    continue
    if [ "$USER" = "guiafoca" ]; then
      echo " Rodando como guia foca"
      break
  else
    echo "não existe numero dir$VAR"
    mkdir /tmp/copia/dir$VAR #cria diretorios
  fi
done

echo " O for foi finalizado"

if [! -f "/tmp/copia/ls"]; then
    cp /bin/ls /tmp/copia
fi
exit 0


obs: Ele deixa de exec o código abaixo do continue e continua exec.
