---
title: 93 - Firewall
created: '2021-05-03T23:46:19.451Z'
modified: '2021-05-03T23:49:06.789Z'
---

# 93 - Firewall

Permitir ou não, o acesso logo ao bater na máquina. 

requisição --> firewall -->tcpwrappers


iptables -A INPUT -s 127.0.0.1 -p tcp --dport 22 -j DROP --> tudo que vier do ip com protocolo tcp na porta 22, dropa

iptables -L -n --> listar em forma númerica 

iptables -D INPUT 1 --> apaga a primeira regra
