---
title: 32 - Administração de Usuários - Gerenciando a senha dos usuários
created: '2021-04-09T14:21:00.740Z'
modified: '2021-04-09T14:29:32.528Z'
---

# 32 - Administração de Usuários - Gerenciando a senha dos usuários 

passwd --help --> exibe opções do passwd 

passwd giropops --> muda a senha e deixa uma mensagem dizendo que a senha é fraca. Se for uma senha fraca e estiver com um usuário root, ele deixa cadastrar. Se for com um usuário comum, ele não vai deixar cadastrar uma senha fraca 

passwd -l giropops --> deixa a conta lockada(travada)

passwd -u giropops --> destrava a conta 

passwd -e giropops --> vai expirar a senha 

passwd -d giropops --> vai deletar a senha

passwd -de giropops --> vai expirar a senha e deletar. Vai exigir uma nova senha ao tentar logar


