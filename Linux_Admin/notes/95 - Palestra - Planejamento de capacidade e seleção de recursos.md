---
title: 95 - Palestra - Planejamento de capacidade e seleção de recursos
created: '2021-05-05T20:02:29.881Z'
modified: '2021-05-05T21:10:31.843Z'
---

# 95 - Palestra - Planejamento de capacidade e seleção de recursos

Requisitos de Hardware

- Funções que ele vai atender
- Tamanho de disco necessário
- Qual o crescimento desse sistema

Requisitos de disco

Como calcular?

- Tamanho total
- Tipo
- Crescimento do que será hospedado(site,bd, logs)
- LVM é seu amigo


Tipos de disco 

-Flash --> 50mbs roteadores embarcados, pouco log e pouca gravação. Ex: pendrive, roteadores

-SATA --> 350mbs baixa performance de gravação 5k RPM- 10k RPM

-SAS --> 450mbs boa performance de gravação 15k RPM. Usados em Storages. Dura 7 anos atuando 24/7

-SSD --> 600mbs excelente performance de gravação/baixa latencia x 32 cmd. Evitar colocar swap pois diminui a vida.

-NVME/M2 --> 6gbs e 64k comandos



Requisitos de memória

- Tamanho de memória(2GB,4GB,8GB..2TB)
- Frequencia(Máquina fisica)

Tem que calcular os níveis de requisições que o servidor terá. É importante ter uma balanceamento de carga.

Alguns serviços e seus requisitos mínimos de memória

- Java --> 4GB no minimo por aplicativo
- dnsmasq --> 1MB
- bins --> 50MB
- dhcp --> 10MB
- apache --> 20MB


CPU

- Frequencia da CPU
- Tamanho do cache L2
- Tipo de operação
  - Residencial - AMD Ryzen 8, i3..
  - Corporativos - Xeon, AMD EPYC


Rede de Comunicação


- Velocidade (10/100/1000/10GB/100GB)
- Tecnologica: cabeada(930max), Fibra, Virtual Switch (Redes virtuais)
- Distancia (Em datacents menor latencia = maquinas mais próximas).
Hospedagem internacional é sempre mais barata























