---
title: 25 - ACPI e teclas de gerencia de energia
created: '2021-04-08T23:25:18.518Z'
modified: '2021-04-08T23:54:20.467Z'
---

# 25 - ACPI e teclas de gerencia de energia

apt-get install acpid --> pacote pra gerenciar o desligamento correto do sistema

cd /etc/acpi/events 

cd /etc/systemd

handledpowerkey=ignore --> pra não desligar ao apertar o botão
