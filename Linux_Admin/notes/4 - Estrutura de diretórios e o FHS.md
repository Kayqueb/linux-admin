---
title: 4 - Estrutura de diretórios e o FHS
created: '2021-01-13T23:10:10.130Z'
modified: '2021-01-20T04:03:10.047Z'
---

# 4 - Estrutura de diretórios e o FHS 

/bin --> binários do sistema. Tudo que são arquivos executáveis. Ex: ls, vim, logout, exit

/boot --> arquivos essênciais para o boot do sistema. 
grub --> inicializador do sistema
vmlinuz --> kernel linux 

/dev --> device ou dispositivos. Ex: sda(hd)

sda --> disco primário(sata ou mais novo)
sda1 --> primeira partição primária
sda2 --> segunda partição
sda3 --> terceira
sda4 --> quarta

sda5 --> primeira partição lógica
sdb --> disco secundário

l(dispositivo de bloco)rw-x

b(dispositivo de bloco)rw-rw --> dispositivo de bloco --> HD, Pendrive guardando informação

c(dispositivo de caracter)rw-rw  --> recebendo e guardando informação.

tty --> dispositivo de caracter, transfere informação. Portas de comunicação 
df -h  --> mostra as partições do HD

/etc  --> diretório onde terá arquivos de configurações. Ex: cd network/
/home --> onde tem os diretórios do usuário
/lib --> onde tem bibliotecas dinamicas de programas e módulos do Kernel. 
   - DLL do Windows. 
   - Módulos = drivers do windows
/lost+found --> Achados e perdidos do Linux. Onde fica o Journal e podemos restaurar o linux caso o pacote quebre.
/media --> diretório onde vai montar seu pendrive em default
/mnt --> ponto de montagem temporário 
/opt --> diretório onde os softwares ficam(ou ficavam, pois não utilizam tanto).
/proc --> filesystem virtual. São informações dinamicas do sistema. Ex: cat meminfo
/root --> diretório home do usuário root
/run --> tudo que está rodando no sistema desde que deu o boot. São dados dinamicos relacionados ao sistema. 
   - tmpfs --> temporary file system
/sbin --> diretório onde terá binarios de uso exclusivo do super usuário. Ex: shutdown, update-passwd
/srv --> Server. Dados que são mais estaticos. Ex: webserver, ftp
/sys --> Parte relacionada ao seu sistema, mas com olhar ao hardware e seus devices, drivers, kernel, module.. 
/tmp --> dados temporários
/usr --> segunda hierarquia de diretórios do linux
/var --> Onde ficam as variáveis do sistema. 
























