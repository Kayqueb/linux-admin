---
title: 162 - Primeiro Shell Script
created: '2021-05-20T22:08:50.329Z'
modified: '2021-05-20T22:22:17.713Z'
---

# 162 - Primeiro Shell Script

mkdir /copia

cp /bin/ls copia

vim copia_arquivos

#!/bin/bsh 
ls
mkdir copia
cp /bin/ls copia


./coṕia_arquivos --> vai dar erro 

chmod 755 copia_arquivos --> dá permissão

obs: se tiver um alias no ls, o script sairá mudado 

vim copia_arquivos

#!/bin/bsh 
ls -la --color=auto
mkdir copia
cp /bin/ls copia

obs: script em modo não interativo. 

- Sem interação

vim copia_arquivos

#!/bin/bsh 
#ls
if [-d "copia"]; then
  echo"o diretorio copia existe, ignorando comando mkdir"
else
  mkdir copia
fi
mkdir copia
cp /bin/ls copia
-----------------------------------


bash -x ./copia_arquivos --> mostra o que foi executado (sinal de +)


- Com interação

#!/bin/bsh 
#ls
if [! -d "copia"]; then
   mkdir copia
fi
cp /bin/ls copia

obs: ! caso não tiver o diretorio cópia ele cria

-----------------------------
Rodar em qualquer diretório que não seja /home













































