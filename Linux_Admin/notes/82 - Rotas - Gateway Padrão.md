---
title: 82 - Rotas - Gateway Padrão
created: '2021-04-17T13:33:44.935Z'
modified: '2021-04-17T13:42:05.434Z'
---

# 82 - Rotas - Gateway Padrão

ip route del default --> remove o gateway 

Só ficará com rotas pra rede local. 

ip route add default via 192.168.0.6 --> cria a rota 

route -n --> mostra a tabela de roteamento IP do kernel

route add default gw 192.168.0.6 --> comando antigo pra adicionar o gateway sec

ip route add default via 192.168.0.6  metric1 --> vai colocar um gw sec. 

metric 0 ganha de metric1












