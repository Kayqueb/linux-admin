---
title: 45 - Redirecionadores - STDEER
created: '2021-04-13T00:44:54.513Z'
modified: '2021-04-13T00:54:46.962Z'
---

# 45 - Redirecionadores - STDEER

2> --> Redireciona a saída de erro padrao - stdeer

lsbla 2> erro.txt

O comando lsbla não existe. Então ele pega o erro e joga dentro do arquivo. Ele substitui a informação dentro do arquivo.



2>> --> Redireciona a saida de erro padrao - stdeer

O comando lsbla não existe. Então ele pega o erro e joga dentro do arquivo. Ele vai concatenar o conteúdo do arquivo.

lsbla 2>> /dev/pts/ --> vai exibir o pts



FD --> File Descriptor 

stdin = 0 
stdout = 1
stderr = 2
