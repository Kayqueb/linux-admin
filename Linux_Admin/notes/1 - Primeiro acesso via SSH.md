---
title: 1 - Primeiro acesso via SSH
created: '2021-01-13T22:08:12.497Z'
modified: '2021-01-13T22:32:54.811Z'
---

# 1 - Primeiro acesso via SSH

ssh usuario@ip_da_máquina(na VM) ou dns

cat /etc/debian_version --> versão do debian

~   -->  diretório /home
pwd --> diretório corrente
$ --> não é usuário root
su -   --> super user(root)
#--> /root
cd  --> mudar de diretório


