---
title: 97 - Troubleshotting - Servidor Web Inoperante
created: '2021-05-05T21:13:55.102Z'
modified: '2021-05-05T21:18:06.839Z'
---

# 97 - Troubleshotting - Servidor Web Inoperante

links 192.168.0.1 --> saber se o serviço abre. 

  - Connection Refused --> informa que não está operante


ssh -A 192.168.0.1 --> conecta no servidor

df -h

entrar na partição 

du -ks * --> quanto espaço na partição está sendo usado

