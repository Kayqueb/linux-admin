---
title: 33 - Administração de Usuários - Gerenciando a senha e os usuários dos grupos
created: '2021-04-09T14:30:33.612Z'
modified: '2021-04-09T14:37:38.078Z'
---

# 33 - Administração de Usuários - Gerenciando a senha e os usuários dos grupos 

gpasswd girus --> vai definir senha para o grupo

gpasswd -a giropops girus --> vai adicionar o usuário giropops ao grupo girus

gpasswd -A girus girus --> vai adicionar o usuário girus como administrador do grupo

gpasswd -d giropops girus --> vai remover o usuário giropops do grupo girus

gpasswd -M girus girus --> vai adicionar o usuário como membro e também como admin do grupo girus


