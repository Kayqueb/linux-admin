---
title: 40 - Permissões - Stick bit
created: '2021-04-09T21:56:48.455Z'
modified: '2021-04-09T22:57:46.029Z'
---

# 40 - Permissões - Stick bit

Usado para diretórios. E apenas o dono daquele arquivo poderá ter poder de escrita. 

/tmp --> stick bit vem por padrão

drwxr-xr-t 

chown jeferson /tmp/tem_stick_aqui.txt

su -giropops 

Não consegue remover

chmod 1755 temp/ --> adiciona o stick bit

chmod 0755 temp/ --> remove o stick
