---
title: 112 - Kernel - Firmwares Binárias e Initramfs
created: '2021-05-06T23:45:13.295Z'
modified: '2021-05-06T23:52:41.614Z'
---

# 112 - Kernel - Firmwares Binárias e Initramfs

Firmwares --> parte binária fornecida pelo fabricante 

cd/lib/firmware

update-initramfs -c -t -k $(uname -r) --> vai tentar construir o initramfs de forma padrão. 

Gera o initrd.img

Adicionar no sources.list  "non-free"


