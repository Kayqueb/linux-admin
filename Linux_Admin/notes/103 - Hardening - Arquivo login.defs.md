---
title: 103 - Hardening - Arquivo login.defs
created: '2021-05-06T16:58:12.735Z'
modified: '2021-05-06T22:08:51.617Z'
---

# 103 - Hardening - Arquivo login.defs

cd /etc/

vi /logim.defs

LOG_OK_LOGINS no

SULOG_FILE  /var/log/sulog --> falhas de logins su

PASS_MAX_DAYS --> maximo pra troca de senha
PASS_WARN_AGE --> warn antes da senha expirar

UID_MIN --> definir o uid
LOGIN_RETRIES 5 --> tentativas de login

DEFAULT_HOME yes --> cd direto pro home

grep motd *





