---
title: 137 - O Comando FSCK
created: '2021-05-18T23:31:53.459Z'
modified: '2021-05-20T14:56:40.671Z'
---

# 137 - O Comando FSCK 

fsck /dev/sdb1 

obs: não faça em arquivos montados

fsck -N /dev/sdb1 --> comando fake

unmount /dev/sdb1
fsck /dev/sdb1 

fsck -A  --> vai passar em todos os discos do /etc/fstab

fsck -AR --> Não vai passar no raiz /root

fsck -M /dev/sdb --> faz um check em discos montados 



GRUB_CMDLINE_LINUX_DEFAULT="quiet fsck.mode=force"

Toda vez que bootar o fsck vai passar


touch forcefsck --> não deixa o raiz ser desmontado














