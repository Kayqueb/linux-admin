---
title: 149 - Adicionando e Removendo chave no Agente
created: '2021-05-20T20:48:48.434Z'
modified: '2021-05-20T21:19:27.704Z'
---

# 149 - Adicionando e Removendo chave no Agente


ssh-add -L --> lista as chaves carregadas

ssh-add -D --> remove

ssh -i .ssh/id_rsa usuario@ip

ssh-keygen -b 4096 -t rsa -C "Chave_guiafoca:17/03/2021" --> Muito segura. 


