---
title: 55 - Gerenciadores de Pacotes - DNF e YUM
created: '2021-04-14T22:57:09.595Z'
modified: '2021-04-15T12:01:02.835Z'
---

# 55 - Gerenciadores de Pacotes - DNF e YUM

São identicos. 

yum install wget --> instalação de pacote

yum remove wget --> remove o pacote

dnf install wget --> instala o pacote

dnf remove wget --> remove o pacote

yum update wget --> faz o upgrade do wget

yum search wget --> pesquisa do wget

dnf search wget --> pesquisa

yum cache wget --> descrição do pacote

yum list installed --> relação de todos os softwares instalados com a versão

yum list --> traz todos os pacotes que podem ser instalados 

yum provides /etc/hostame --> traz o pacote que ele pertence 

yum grouplist --> traz a lista de grupos disponíveis para instalar 

yum groupinstall "Container Management" --> instala todo o grupo necessário pra rodas containers

yum groupremove "Container Management" --> remove tudo.

yum repolist --> traz os repos configurados 

yum repolist all --> traz todos os repos que podem ser instalados

yum 














