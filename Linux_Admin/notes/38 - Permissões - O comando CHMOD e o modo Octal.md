---
title: 38 - Permissões - O comando CHMOD e o modo Octal
created: '2021-04-09T20:06:04.325Z'
modified: '2021-04-09T21:42:04.681Z'
---

# 38 - Permissões - O comando CHMOD e o modo Octal

chmod 644 teste --> rw r r

chmod 511 teste_dir/ --> Se houver uma estrutura de diretórios, só modifica o diretório corrente

chmod -R 511 teste_dir/ --> Modifica todos os diretórios. De forma recursiva.
