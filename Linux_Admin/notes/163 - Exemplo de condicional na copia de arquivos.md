---
title: 163 - Exemplo de condicional na copia de arquivos
created: '2021-05-20T22:23:50.829Z'
modified: '2021-05-20T22:26:37.693Z'
---

# 163 - Exemplo de condicional na copia de arquivos

#!/bin/bsh 
#ls
if [-d "/tmp/copia"]; then
    mkdir /tmp/copia
fi
if [! -f "/tmp/copia/ls"]; then
    cp /bin/ls /tmp/copia
fi

Obs: Se não houver o diretoria existe e se não existe, ele copia. 
