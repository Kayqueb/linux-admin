---
title: 165 - Variáveis
created: '2021-05-20T22:43:31.795Z'
modified: '2021-05-20T22:56:05.730Z'
---

# 165 - Variáveis

#!/bin/bsh 
#ls
echo -n "Entre com o nome de usuario: "
read USUARIO

if ["$USER" != "$USUARIO"]; then
    echo "Nao esta rodando como $USUARIO"
    exit 1
fi 

exit 0

if [-d "/tmp/copia"]; then
    mkdir /tmp/copia
fi
if [! -f "/tmp/copia/ls"]; then
    cp /bin/ls /tmp/copia
fi
exit 0

aspas simples --> não interpreta o conteúdo da variável
