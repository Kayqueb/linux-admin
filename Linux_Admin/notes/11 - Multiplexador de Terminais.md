---
title: 11 - Multiplexador de Terminais
created: '2021-03-19T22:20:41.506Z'
modified: '2021-03-24T00:37:27.437Z'
---

# 11 - Multiplexador de Terminais

screen 

ctrl+a --> no other window 
ctrl+a+c --> abre um novo terminal 
ctrl+a+a --> alterna tela de terminal
ctrl+a+d --> desconecta da screen
screen -x --> reconecta na screen

tmux new --> nova sessão
ctrl+b+c --> nova tela
ctrl+b+0 --> vai pra tela 
ctrl+d --> desconecta da screen
tmux attach --> conecta na screen
ctrl+b+ seta pra cima e baixo --> alterna as telas
