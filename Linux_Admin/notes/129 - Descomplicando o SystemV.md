---
title: 129 - Descomplicando o SystemV
created: '2021-05-18T20:52:53.670Z'
modified: '2021-05-18T20:56:55.586Z'
---

# 129 - Descomplicando o SystemV

vim /etc/inittab --> não existe mais. Mas definia o runlevel

id:2:initdefault:  --> você pode alterar qual será o init


0 --> halt
1 --> single user

2-5 --> multi-user

6 --> reboot


