---
title: 94 - Comandos de Rede
created: '2021-05-03T23:50:12.206Z'
modified: '2021-05-05T20:01:32.077Z'
---

# 94 - Comandos de Rede

who --> mostra conexões realizadas na máquina

who -a --> mostra todos os detalhes de conexão

who -b --> horário da inicialização do sistema

who -s --> formato mais curto 

who -q --> nome de usuários e total de users conectados 

telnet --> inseguro

finger guiafoca --> pega todos os dados do login no passwd e também dados de conexão

(inseguro para estar na máquina)

ftp http.us.debian.org --> acessa ao servidor ftp

whoami --> qual usuario está conectado

dnsdomainname --> retorna o dns

Os dominios dns locais precisam ser especificados no /etc/hosts
ip foca01.guiafoca.org foca01


ping img --> se ele não localizar o dominio completo, ele vai resolver o dominio 

hostnamectl --> vai exibir detalhes do host

write --> permite mandar uma mensagem na tty1

write root tty1 --> enviar a mensagem pro root na tty1

echo "O sistema será reiniciado em 10 min" | wall --> broadcast de uma mensagem pra todos os usuários conectados
















