---
title: 41 - Permissões - SUID e SGID
created: '2021-04-09T22:57:53.406Z'
modified: '2021-04-09T23:06:39.252Z'
---

# 41 - Permissões - SUID e SGID

SUID --> Todo e qualquer arquivo binário. Qualquer pessoa que executará como dono do arquivo.

Ex: /bin/su --> executando como se fosse o root 


