---
title: 150 - Authorized_keys
created: '2021-05-20T21:20:32.925Z'
modified: '2021-05-20T21:23:54.440Z'
---

# 150 - Authorized_keys

scp -P 22 /home/usuario/.ssh/id_rsa.pub guiafoca@ip:/diretóri/destino --> chave copiada pra máquina remota

cd /ssh/

cat authorized_keys --> fica guardada a chave

cat /tmp/id_rsa.pub >> authorized_keys --> copia para o final do arquivo
