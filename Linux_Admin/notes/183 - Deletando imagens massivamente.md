---
title: 183 - Deletando imagens massivamente
created: '2021-05-24T20:47:22.471Z'
modified: '2021-05-24T20:54:59.192Z'
---

# 183 - Deletando imagens massivamente

for i in $(sql 8000 8040);do echo $i; docker container run -d -p $i:80 --name nginx-$i nginx:latest ;done --> vai baixar diversas imagens

for i in $(sql 8000 8040);do echo $i; docker container rm -f nginx-$i nginx:latest ;done --> vai baixar diversas imagens -- remove todos os cotainers


docker container prune --> remove todos os parados 

docker image prune --> remove todas as imagens 

docker system prune --> remove todos os containers parados 




