---
title: 48 - Operadores AND && e o OR ||
created: '2021-04-13T01:07:06.804Z'
modified: '2021-04-13T01:11:54.480Z'
---

# 48 - Operadores AND && e o OR ||

AND 
&&       

ls opa.txt && mv opa.txt novo_opa.txt

Se o primeiro comando funcionar, você executa o segundo. 


OR 
||

ls novo_opa.txt || ls opa.txt || ls arq_novo.txt 

Ele vai executando enquanto está errado. Quando estiver certo, ele para. 
